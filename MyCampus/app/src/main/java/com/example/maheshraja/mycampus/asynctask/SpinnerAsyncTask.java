package com.example.maheshraja.mycampus.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by lenovo on 15-07-2016.
 */
public class SpinnerAsyncTask extends AsyncTask<String, Void, String>
{
    private String jsonData;
    private AsyncTaskCompleteListener asyncTaskCompleteListener;
    private int connectionTimeout;
    private int socketTimeout;

    public SpinnerAsyncTask( AsyncTaskCompleteListener asyncTaskCompleteListener, int connectionTimeout, int socketTimeout) {

        this.asyncTaskCompleteListener = asyncTaskCompleteListener;
        this.connectionTimeout = connectionTimeout;
        this.socketTimeout = socketTimeout;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        HttpURLConnection conn = null;
        try {
            URL myurl = new URL(MyConstants.APP_URL+params[0]);
            System.out.println("myurl is :"+myurl);

            conn = (HttpURLConnection) myurl.openConnection();
            conn.setConnectTimeout(connectionTimeout);
            conn.setReadTimeout(socketTimeout);
            conn.setDoInput(true);
            conn.setRequestMethod("GET");

           /* OutputStream os = conn.getOutputStream();
            os.write(jsonData.getBytes());
            os.flush();
*//*
            OutputStream os = conn.getOutputStream();
            os.write(jsonData.getBytes("UTF-8"));
            os.close();*/


            InputStream inputStream = conn.getInputStream();
            String responseStr = Utility.convertStreamToString(inputStream);
            Log.i("Async", responseStr);
            return responseStr;
        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            if (conn != null)
                conn.disconnect();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        asyncTaskCompleteListener.onAsynComplete(result);
    }
}
