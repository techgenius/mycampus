package com.example.maheshraja.mycampus.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.asynctask.JobportalAsynTask;
import com.example.maheshraja.mycampus.asynctask.SearchJobAsynTask;
import com.example.maheshraja.mycampus.dtos.DTO;
import com.example.maheshraja.mycampus.dtos.JobPortalDTO;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 20-07-2016.
 */

public class Searchforjob extends  BaseFragment {
    private EditText edit;
    String ID;
    Button button;
    static Jobsearchlistview result;

    List<JobPortalDTO> list = new ArrayList<>();
    List<String> lables = new ArrayList<>();
    List<JobPortalDTO> list1 = new ArrayList<>();
    ArrayList<String> lables1= new ArrayList<>();
private Context context;
    JobPortalDTO st;
    String keyword;
    String expirence;
    String conmsg;
    ArrayAdapter<String> spinnerAdapter;
    JSONObject obj;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.search_fragment, container, false);
        context=mActivity;

        final Spinner type = (Spinner) rootView.findViewById(R.id.list);
        final EditText edit=(EditText)rootView.findViewById(R.id.location);
        System.out.println("hai");
        getSponsers();
        System.out.println("hello");



        System.out.println("swarupa is:" + lables);

        final Spinner sp3= (Spinner)rootView.findViewById(R.id.list1);
        ArrayAdapter<CharSequence> adp3=ArrayAdapter.createFromResource(context,
                R.array.spinnerItems, android.R.layout.simple_list_item_1);

        adp3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp3.setAdapter(adp3);


        spinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, lables);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        type.setAdapter(spinnerAdapter);
        type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("" + parent.getItemAtPosition(position).toString());
                Toast.makeText(context, "onitem click postion" + parent.getItemAtPosition(position).toString(), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        button = (Button) rootView.findViewById(R.id.submit);



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit.getText().toString().trim().length() > 0) {
                    Toast.makeText(context, "OnClickListener : " + "\nSpinner 1 : " + String.valueOf(type.getSelectedItem()), Toast.LENGTH_SHORT).show();
                    keyword = String.valueOf(type.getSelectedItem());

                    expirence = String.valueOf(sp3.getSelectedItem());
                    System.out.println("keyword is:" + keyword);
                    System.out.println("expirence is:" + expirence);

                    conmsg = edit.getText().toString().trim();
                    System.out.println("location  is:" + conmsg);
                    sendData();
                } else {
                    Utility.showAlert(mActivity, null, "Please enter your message");
                }
            }
        });


        return rootView;
    }
    private void sendData() {

        JobPortalDTO dto = (JobPortalDTO) getUserData();

        JSONObject reqJson = new JSONObject();

        try {
            reqJson.put("keyword", dto.getKeyword());
            reqJson.put("wanted_exp", dto.getWanted_exp());
            reqJson.put("interview_loc", dto.getInterview_loc());

        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){

        }

        SearchJobAsynTask loginAsync = new SearchJobAsynTask(reqJson.toString(), taskCompleteListener, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
        Utility.showProgressDialog(context);
        loginAsync.execute(MyConstants.JOBPORTAL_GETJOBS_URL);

    }

    private DTO getUserData() {

        JobPortalDTO dto = new JobPortalDTO();
        dto.setKeyword(keyword);
        dto.setWanted_exp(expirence);
        dto.setInterview_loc(conmsg);

        return  dto;
    }
    private void getSponsers() {
        System.out.println("getSponser is calling");
        JobportalAsynTask sponser = new JobportalAsynTask(completeListener, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
        Utility.showProgressDialog(context);
        sponser.execute(MyConstants.JobPoral_SEARCHJOB_URL);
    }

    private void populateData() {

        spinnerAdapter.notifyDataSetChanged();
    }


    private AsyncTaskCompleteListener completeListener = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result) {
            if (result != null && !result.isEmpty()) {
                if (Utility.isJSONValid(result)) {
                    System.out.println("test AsyncTask for retrive");
                    try {


                        JSONArray ar = new JSONArray(result);
                        if (ar != null && ar.length() > 0) {
                            for (int i = 0; i < ar.length(); i++) {
                                JSONObject obj = ar.getJSONObject(i);
                                JobPortalDTO dto = new JobPortalDTO();
                                dto.setKeyword(obj.getString("keyword"));
                                System.out.println("ar=" + ar);


                                list.add(dto);
                            }
                            if (list.size() > 0) {
                                for (int ii = 0; ii < list.size(); ii++) {
                                    lables.add(list.get(ii).getKeyword());
                                    populateData();
                                    System.out.println("display lable" + lables);
                                }


                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {
                    //Utility.showAlert(LoginActivity.this, null, "Invalid Response contact system admin");
                    //Utility.showAlert(SponsorsFragment.this, null, "Invali userId/password");
                }

            } else {
                //Utility.showAlert(LoginActivity.this, "Network problem", "Check your internet connection");
                // Utility.showAlert(LoginActivity.this, null, "Invali userId/password");
            }
            Utility.hideProgressDialog();
        }
    };


private AsyncTaskCompleteListener taskCompleteListener = new AsyncTaskCompleteListener() {
    @Override
    public void onAsynComplete(String result1) {
        if (result1 != null && !result1.isEmpty()) {
            if (Utility.isJSONValid(result1)) {

                System.out.println("test AsyncTask for retrive");
                try {


                    JSONArray ar = new JSONArray(result1);
                    if (ar != null && ar.length() > 0) {
                        for (int i = 0; i < ar.length(); i++) {
                    obj= ar.getJSONObject(i);
                            JobPortalDTO dto = new JobPortalDTO();
                            dto.setCompany_name(obj.getString("company_name"));
                           dto.setCompany_loc(obj.getString("company_location"));
                            dto.setInterview_loc(obj.getString("interview_loc"));


                            dto.setKeyword(obj.getString("keyword"));

                            dto.setTagline(obj.getString("tagLine"));
                            dto.setWanted_exp(obj.getString("wanted_exp"));
                            dto.setJobdescription(obj.getString("jobdescription"));
dto.setJobId(obj.getString("jobId"));
                            System.out.println("ar=" + ar);


                            list1.add(dto);





                        }
                        if(list1.size()>0)
                        {
                            for (int ii = 0; ii < list1.size(); ii++) {
                                lables1.add(list1.get(ii).getCompany_loc());
                                lables1.add(list1.get(ii).getCompany_name());
                                lables1.add(list1.get(ii).getInterview_loc());
                                lables1.add(list1.get(ii).getKeyword());
                                lables1.add(list1.get(ii).getTagline());
                                lables1.add(list1.get(ii).getWanted_exp());
                                System.out.println("lables1 is:" + lables1);
                                FragmentTransaction transection=getFragmentManager().beginTransaction();
                                Jobsearchlistview mfragment=new Jobsearchlistview();
//using Bundle to send data
                                Bundle bundle=new Bundle();
                             //bundle.putStringArrayList("mylist",list1);
                                bundle.putSerializable("array", (java.io.Serializable) list1);
                                mfragment.setArguments(bundle); //data being send to SecondFragment
                                transection.replace(R.id.container_body, mfragment);
                                transection.commit();
                            }

                        }


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else {
                //Utility.showAlert(LoginActivity.this, null, "Invalid Response contact system admin");
                //Utility.showAlert(SponsorsFragment.this, null, "Invali userId/password");
            }

        } else {
            //Utility.showAlert(LoginActivity.this, "Network problem", "Check your internet connection");
            // Utility.showAlert(LoginActivity.this, null, "Invali userId/password");
        }
        Utility.hideProgressDialog();
    }
};

}