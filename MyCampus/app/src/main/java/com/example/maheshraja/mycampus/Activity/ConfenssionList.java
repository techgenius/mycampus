package com.example.maheshraja.mycampus.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.adapter.Confessionlistadapter;
import com.example.maheshraja.mycampus.asynctask.SpinnerAsyncTask;
import com.example.maheshraja.mycampus.dtos.ConfessionDTO;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by lenovo on 16-07-2016.
 */
public class ConfenssionList  extends BaseFragment {
    String ID;
    private TabLayout mTabLayout;
    private Context context;
    List<ConfessionDTO> list= new ArrayList<ConfessionDTO>();

    ConfessionDTO dto;
    Confessionlistadapter adapter;
    private ViewPager mViewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = mActivity;
        View rootView = inflater.inflate(R.layout.fragment_confenssionlist, container, false);


        getSponsers();

        ListView tx=  (ListView) rootView.findViewById(R.id.sponser);
        adapter = new Confessionlistadapter(list,context);
        tx.setAdapter(adapter);

        Button butn=(Button) rootView.findViewById(R.id.addbutton);

        butn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.pushFragments(MyConstants.TAB_HOME, new Confessions(), false, true);
            }
        });


        return rootView;
    }
        private void setupTabLayout(TabLayout tabLayout) {
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(mViewPager);
    }
    private void getSponsers() {
        System.out.println("getSponser is calling");
        SpinnerAsyncTask sponser = new SpinnerAsyncTask(completeListener, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
        Utility.showProgressDialog(context);
        sponser.execute(MyConstants.ADVT_URL);
    }

    private void populateData() {

        adapter.notifyDataSetChanged();
    }

    private AsyncTaskCompleteListener completeListener = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result) {
            if (result != null && !result.isEmpty()) {
                if (Utility.isJSONValid(result)) {
                    System.out.println("test AsyncTask for retrive");
                    try {


                        JSONArray ar = new JSONArray(result);
                        if (ar != null && ar.length() > 0) {
                            for (int i = 0; i < ar.length(); i++) {
                                JSONObject obj = ar.getJSONObject(i);
                                 dto = new ConfessionDTO();
                                dto.setData(obj.getString("data"));
                                dto.setCollege_name(obj.getString("college_name"));
                                dto.setConfession_id(obj.getInt("confession_id"));
                                dto.setLikecount(obj.getInt("likesCount"));
                                System.out.println("likecount is:=" + obj.getInt("likesCount"));

                                System.out.println("ar=" + ar);


                                list.add(dto);




                                }
                            if (list.size() > 0) {

                                System.out.println("list size is:"+list.size());
                                populateData();

                            }


                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {
                    //Utility.showAlert(LoginActivity.this, null, "Invalid Response contact system admin");
                    //Utility.showAlert(SponsorsFragment.this, null, "Invali userId/password");
                }

            } else {
                //Utility.showAlert(LoginActivity.this, "Network problem", "Check your internet connection");
                // Utility.showAlert(LoginActivity.this, null, "Invali userId/password");
            }
            Utility.hideProgressDialog();
        }
    };


}
