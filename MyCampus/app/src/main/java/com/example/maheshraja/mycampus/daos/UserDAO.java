package com.example.maheshraja.mycampus.daos;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.maheshraja.mycampus.dtos.DTO;
import com.example.maheshraja.mycampus.dtos.UserDTO;

import java.util.ArrayList;
import java.util.List;




public class UserDAO implements DAO {
    private final String TAG = "userDAO";
    private static UserDAO userDAO;


    public static UserDAO getInstance() {
        if (userDAO == null) {
            userDAO = new UserDAO();
        }

        return userDAO;
    }

    /**
     * delete the Data
     */


    @Override
    public boolean delete(DTO dtoObject, SQLiteDatabase dbObject) {

        try {
            dbObject.compileStatement("DELETE FROM USER").execute();
            return true;
        } catch (Exception e) {
            Log.e(TAG + "delete()", e.getMessage());
        }
        return false;
    }


    @Override
    public DTO getRecordsById(long id, SQLiteDatabase dbObject) {
        return null;
    }

    /**
     * Inserts the data in the SQLite database
     *
     * @param dbObject  : Exposes methods to manage a SQLite database Object
     * @param dtoObject : DTO object is passed
     */
    @Override
    public boolean insert(DTO dtoObject, SQLiteDatabase dbObject) {

        try {
            UserDTO dto = (UserDTO) dtoObject;

            ContentValues cValues = new ContentValues();

            /*USER
            id
            name
            mobileNo
            email
            gender
            address
            uid
            uidType
            dob
            location
            */


            cValues.put("name", dto.getName());
            cValues.put("mobileNo", dto.getMobileNo());
            cValues.put("email", dto.getEmailId());
            cValues.put("gender", dto.getGender());
            cValues.put("address", dto.getAddress());
            cValues.put("uid", dto.getuId());
            cValues.put("uidType", dto.getuIdType());
            cValues.put("dob", dto.getAge());
            cValues.put("location", dto.getLocation());
            cValues.put("password", dto.getPassword());

            dbObject.insert("USER", null, cValues);
            return true;
        } catch (SQLException e) {
            Log.e(TAG + "insert()", e.getMessage());
            return false;

        } finally {
            dbObject.close();
        }

    }


    @Override
    public boolean update(DTO dtoObject, SQLiteDatabase dbObject) {

        try {
            UserDTO dto = (UserDTO) dtoObject;

            ContentValues cValues = new ContentValues();

            /*USER
            id
            name
            mobileNo
            email
            gender
            address
            uid
            uidType
            dob
            location
            */


            if (dto.getName() != null)
                cValues.put("name", dto.getName());
            if (dto.getMobileNo() != null)
                cValues.put("mobileNo", dto.getMobileNo());
            if (dto.getEmailId() != null)
                cValues.put("email", dto.getEmailId());
            if (dto.getGender() != null)
                cValues.put("gender", dto.getGender());
            if (dto.getAddress() != null)
                cValues.put("address", dto.getAddress());
            if (dto.getuId() != null)
                cValues.put("uid", dto.getuId());
            if (dto.getuIdType() != null)
                cValues.put("uidType", dto.getuIdType());
            if (dto.getAge() != null)
                cValues.put("dob", dto.getAge());
            if (dto.getLocation() != null)
                cValues.put("location", dto.getLocation());
            if(dto.getPassword() != null)
                cValues.put("password", dto.getPassword());

            dbObject.update("USER", cValues, null, null);
            return true;
        } catch (SQLException e) {
            Log.e(TAG + "insert()", e.getMessage());
            return false;

        } finally {
            dbObject.close();
        }
    }


    @Override
    public List<DTO> getRecords(SQLiteDatabase dbObject) {
        List<DTO> userInfo = new ArrayList<DTO>();
        Cursor cursor = null;
        try {

            cursor = dbObject.rawQuery("SELECT * FROM USER", null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
           /*USER
            id
            name
            mobileNo
            email
            gender
            address
            uid
            uidType
            dob
            location
            */
                    UserDTO dto = new UserDTO();

                    //dto.setId(cursor.getLong(0));
                    dto.setName(cursor.getString(1));
                    dto.setMobileNo(cursor.getString(2));
                    dto.setEmailId(cursor.getString(3));
                    dto.setGender(cursor.getString(4));
                    dto.setAddress(cursor.getString(5));
                    dto.setuId(cursor.getString(6));
                    dto.setuIdType(cursor.getString(7));
                    dto.setAge(cursor.getString(8));
                    dto.setLocation(cursor.getString(9));
                    dto.setPassword(cursor.getString(10));

                    userInfo.add(dto);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            dbObject.close();

        }

        return userInfo;
    }

    @Override
    public List<DTO> getRecordInfoByValue(String columnName, String columnValue, SQLiteDatabase dbObject) {
        List<DTO> userInfo = new ArrayList<DTO>();
        Cursor cursor = null;
        try {
            if (!(columnName != null && columnName.length() > 0))
                columnName = "parentId";

            cursor = dbObject.rawQuery("SELECT * FROM USER where " + columnName + " = '" + columnValue + "'", null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
           /*USER
            id
            name
            mobileNo
            email
            gender
            address
            uid
            uidType
            dob
            location
            */
                    UserDTO dto = new UserDTO();

                    //dto.setId(cursor.getLong(0));
                    dto.setName(cursor.getString(1));
                    dto.setMobileNo(cursor.getString(2));
                    dto.setEmailId(cursor.getString(3));
                    dto.setGender(cursor.getString(4));
                    dto.setAddress(cursor.getString(5));
                    dto.setuId(cursor.getString(6));
                    dto.setuIdType(cursor.getString(7));
                    dto.setAge(cursor.getString(8));
                    dto.setLocation(cursor.getString(9));
                    dto.setPassword(cursor.getString(10));

                    userInfo.add(dto);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            dbObject.close();

        }

        return userInfo;
    }

    public boolean isDataAvailable(SQLiteDatabase dbObject) {
        Cursor cursor = null;
        try {

            cursor = dbObject.rawQuery("SELECT count(name) FROM USER", null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                if (cursor.getInt(0) > 0)
                    return true;
                else
                    return false;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return false;
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            dbObject.close();
        }

        return false;
    }
}