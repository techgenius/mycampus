package com.example.maheshraja.mycampus.dtos;

import java.util.List;

/**
 * Created by swamy on 22-05-2016.
 */
public class FriendsListDTO {

    private String userId;
    private List<FriendsDTO> friends;
    private String authcode;

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<FriendsDTO> getFriends() {
        return friends;
    }

    public void setFriends(List<FriendsDTO> friends) {
        this.friends = friends;
    }

}
