package com.example.maheshraja.mycampus.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.asynctask.MyAsyncTask;
import com.example.maheshraja.mycampus.daos.UserDAO;
import com.example.maheshraja.mycampus.database.DBHandler;
import com.example.maheshraja.mycampus.dtos.DTO;
import com.example.maheshraja.mycampus.dtos.UserDTO;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class OTPActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtOTP;
    private Context context;
    private Button btnSubmit;
    private TextView txResendOTP;
    private CountDownTimer timer;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        context = this;

        email = getIntent().getStringExtra("email");

        edtOTP = (EditText) findViewById(R.id.otp_otp);
        btnSubmit = (Button) findViewById(R.id.otp_submit);
        txResendOTP = (TextView) findViewById(R.id.otp_resend);
        txResendOTP.setOnClickListener(this);
        startResendTimer();

        btnSubmit.setOnClickListener(this);

    }

    private void startResendTimer() {
        txResendOTP.setVisibility(View.GONE);
        timer = new CountDownTimer(60 * 1000, 60 * 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                txResendOTP.setVisibility(View.VISIBLE);
            }
        }.start();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.otp_submit) {
            if (edtOTP.getText().toString().trim().length() == 4) {
                sendReq();
            } else {
                Utility.showAlert(context, null, "Enter valid OTP");
            }
        } else if (v.getId() == R.id.otp_resend) {
            String reqData = "{\"mailid\":\"" + email + "\"}";
            MyAsyncTask asyncTask = new MyAsyncTask(reqData, onComplete, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
            asyncTask.execute(MyConstants.RESEND_URL);
        }
    }

    private AsyncTaskCompleteListener onComplete = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result) {
            if (result != null) {
                if (Utility.isJSONValid(result)) {
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.optInt("code");
                        if (code == 100) {
                            Utility.showAlert(OTPActivity.this, null, "Requested Successfully");
                            startResendTimer();
                        } else if (code == 101) {
                            Utility.showAlert(OTPActivity.this, null, "Failed Request");
                        } else {
                            Utility.showAlert(OTPActivity.this, null, "Unknown response");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utility.showAlert(OTPActivity.this, null, "Invalid Response contact system admin");
                }

            } else {
                Utility.showAlert(OTPActivity.this, "Network problem", getString(R.string.check_connection));
            }
            Utility.hideProgressDialog();
        }
    };

    private void sendReq() {
        String url = MyConstants.OTP_URL;
        if (Utility.networkAvailability(context)) {
            String reqData = getRequestData();
            MyAsyncTask asyncTask = new MyAsyncTask(reqData, onAsyncComplete, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
            Utility.showProgressDialog(context);
            asyncTask.execute(url);
        } else {
            Utility.showAlert(context, null, getString(R.string.check_connection));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null)
            timer.cancel();
    }

    private AsyncTaskCompleteListener onAsyncComplete = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result) {
            if (result != null) {
                if (Utility.isJSONValid(result)) {
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.optInt("code");
                        if (code == 100) {
                            Utility.setOTPComplete(true, OTPActivity.this);
                            navigateApp();
                        } else if (code == 101) {
                            Utility.showAlert(context, null, "Invalid OTP");
                        } else {
                            Utility.showAlert(context, null, "Unknown response");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utility.showAlert(context, null, "Invalid Response contact system admin");
                }
            } else {
                Utility.showAlert(context, "Network problem", "Check your internet connection");
            }
            Utility.hideProgressDialog();
        }
    };

    private String getRequestData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mailid", email);
            jsonObject.put("mobileOTP", edtOTP.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return jsonObject.toString();
    }

    private void showProgress() {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        new CountDownTimer(500, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                progressDialog.cancel();
                navigateApp();
            }
        }.start();

    }

   private void navigateApp() {
        if(timer != null)
            timer.cancel();
       Intent intent = new Intent(OTPActivity.this, MainActivity.class);
        intent.putExtra("email", email);
        finish();
        startActivity(intent);

    }
}