package com.example.maheshraja.mycampus.listener;

/**
 * Created by Swamy on 3/4/2016.
 */
public interface AsyncTaskCompleteListener {
    public void onAsynComplete(String result);
}
