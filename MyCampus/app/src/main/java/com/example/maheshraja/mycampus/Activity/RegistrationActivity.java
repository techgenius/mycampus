package com.example.maheshraja.mycampus.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.asynctask.ImageAsyncTask;
import com.example.maheshraja.mycampus.asynctask.MyAsyncTask;
import com.example.maheshraja.mycampus.daos.UserDAO;
import com.example.maheshraja.mycampus.database.DBHandler;
import com.example.maheshraja.mycampus.dtos.DTO;
import com.example.maheshraja.mycampus.dtos.UserDTO;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class RegistrationActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private Button btnSubmit;

    private EditText userNameEdt, emailIdEdt, mobileNoEdt, passwordEdt, confPasswordEdt, addressEdt, dobEdt, uidEdt, locationEdt;

    private RadioGroup genderRadio;

    private Context context;

    private Toolbar mToolbar;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private String TAG = "RegistrationActivity";
    private String location;
    private CountDownTimer mLocationCountDownTimer;
    private ProgressDialog dialog;
    protected final int COUNT_DOWN_TIME = 30000;
    protected final int COUNT_DOWN_TIME_INTERVAL = 1000;
    protected static boolean checkForLocation = true;
    private String current;
    private File mypath;
    private Uri uriSavedImage;
    private final int IMAGECAPTURE_ACTIVITY = 100;
    private String imgFilePath = "";

    private ImageView mCameraImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);

        context = this;

        //Mint.initAndStartSession(RegistrationActivity.this, "89a68d2b");

        if (Utility.checkPlayServices(context)) {
            buildGoogleApiClient();
        }

        btnSubmit = (Button) findViewById(R.id.reg_submit);

        userNameEdt = (EditText) findViewById(R.id.reg_name);
        mobileNoEdt = (EditText) findViewById(R.id.reg_mobile);
        emailIdEdt = (EditText) findViewById(R.id.reg_email);
        addressEdt = (EditText) findViewById(R.id.reg_addr);
        uidEdt = (EditText) findViewById(R.id.reg_uid);
        locationEdt = (EditText) findViewById(R.id.reg_location);
        genderRadio = (RadioGroup) findViewById(R.id.reg_gender);
        passwordEdt = (EditText) findViewById(R.id.reg_password);
        confPasswordEdt = (EditText) findViewById(R.id.reg_conf_password);
        dobEdt = (EditText) findViewById(R.id.reg_dob);
        mCameraImg = (ImageView) findViewById(R.id.reg_profile_img);

        mCameraImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                captureImage();
                dispatchTakePictureIntent();
            }

        });

        /*dobEdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePickerDailog();
            }
        });*/

        /*dobEdt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    openDatePickerDailog();
                }
            }
        });*/


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String validation = validateFields();
                if (validation == null) {
                    if (location != null && !location.isEmpty()) {
                        if (Utility.networkAvailability(context)) {
//                        showProgress();
                            registrationProcess();
//                            loginProcess();
                        } else {
                            Utility.showAlert(context, null, getString(R.string.check_connection));
                        }
                    } else {
                        getCurrentLocation(context);
                    }
                } else {
                    Utility.showAlert(context, "Alert", validation);
                }
                //uploadImage();
            }
        });

    }

    private void uploadImage() {
        ImageAsyncTask imageAsyncTask = new ImageAsyncTask(context, Utility.getFileDir(context)+ File.separator+Utility.getProfileImageURL(context), taskCompleteListener, MyConstants.IMG_CONNECTION_TIMEOUT, MyConstants.IMG_SOCKET_TIMEOUT);
        Utility.showProgressDialog(context);
        imageAsyncTask.execute(MyConstants.IMG_UPLOAD_URL);
    }

    private AsyncTaskCompleteListener taskCompleteListener = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result) {
            if (result != null) {
                if(Utility.isJSONValid(result)){
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.optInt("code");
                        if(code == 100){
                            navigateApp();
                            Utility.showAlert(context, null, "Updated Your Profile Successfully");
                        }else if(code == 101){
                            Utility.showAlert(context, null, "Failed Updated Your Profile");
                        }else{
                            Utility.showAlert(context, null, "Unknown response");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Utility.showAlert(context, null, "Invalid Response contact system admin");
                }

            } else {
                Utility.showAlert(context, "Network problem", "Check your internet connection");
            }
            Utility.hideProgressDialog();
        }
    };

    private void openDatePickerDailog() {
        Calendar c = Calendar.getInstance();
        MyDatePicker datePicker = new MyDatePicker();
        new DatePickerDialog(context, datePicker, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show();

    }

    class MyDatePicker implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            dobEdt.setText("" + dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
        }
    }

    private void registrationProcess() {

        UserDTO dto = (UserDTO) getUserData();

        JSONObject reqJson = new JSONObject();

        try {
            reqJson.put("deviceid", Utility.getDeviceId(RegistrationActivity.this));
            reqJson.put("password", dto.getPassword());
            reqJson.put("address", dto.getAddress());
            reqJson.put("gender", dto.getGender());
            reqJson.put("latlan", dto.getLocation());
            reqJson.put("mailid", dto.getEmailId());
            reqJson.put("mobilenum", dto.getMobileNo());
            reqJson.put("name", dto.getName());
            reqJson.put("age", dto.getAge());
            reqJson.put("imei", getIMEINo(context));

        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){

        }

        MyAsyncTask loginAsync = new MyAsyncTask(reqJson.toString(), completeListener, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
        Utility.showProgressDialog(context);
        loginAsync.execute(MyConstants.REG_URL);

    }

    private AsyncTaskCompleteListener completeListener = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result) {
            if (result != null) {
                if(Utility.isJSONValid(result)){
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.optInt("code");


                        if(code == 100){
                            Utility.setAuthCode(object.optString("authcode"),RegistrationActivity.this);
                            saveData(getUserData());
                            uploadImage();
                            return;
                        }else if(code == 101){
                            Utility.showAlert(context, null, "Failed Register");
                        }else{
                            Utility.showAlert(context, null, "Unknown response");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Utility.showAlert(context, null, "Invalid Response contact system admin");
                }

            } else {
                Utility.showAlert(context, "Network problem", "Check your internet connection");
            }
            Utility.hideProgressDialog();
        }
    };

    private String getURL() {
        DTO mainDTO = getUserData();
        UserDTO dto = (UserDTO) mainDTO;

        String str = "http://bighand.in/reg.php?name=" + dto.getName() + "&cellno=" + dto.getMobileNo() + "&gender=" + dto.getGender() + "&longitude=78.4006983&lattitude=17.4590091&emailid=" + dto.getEmailId() + "&deviseid=" + Utility.getDeviceId(RegistrationActivity.this);

        return str;
    }

    private void saveData(DTO userData) {
        context.deleteDatabase("security.db");
        UserDAO.getInstance().insert(userData, DBHandler.getInstance(context).getDBObject(1));
        //navigateApp();
    }

    private DTO getUserData() {
        UserDTO dto = new UserDTO();
        dto.setName(getText(userNameEdt));
        dto.setMobileNo(getText(mobileNoEdt));
        dto.setEmailId(getText(emailIdEdt));
        dto.setLocation(location);
        dto.setAddress(getText(addressEdt));
        dto.setuId(Utility.getDeviceId(context));
        dto.setGender(((RadioButton) findViewById(genderRadio.getCheckedRadioButtonId())).getText().toString().trim());
        dto.setPassword(getText(passwordEdt));
        dto.setAge(getText(dobEdt));


        return dto;
    }

    private String validateFields() {
        if (isTextEmpty(userNameEdt)) {
            return "Enter your name";
        }

        if (isTextEmpty(mobileNoEdt)) {
            return "Enter mobile number";
        }

        if (mobileNoEdt.getText().toString().trim().length() <= 9) {
            return "Enter valid mobile number";
        }

        if (isTextEmpty(emailIdEdt)) {
            return "Enter mail ID";
        }

        if(!isValidEmailAddress(getText(emailIdEdt))){
            return "Enter valid mail ID";
        }

        if (isTextEmpty(passwordEdt)) {
            return "Enter app password";
        }

        if (getText(passwordEdt).length() < 6) {
            return "Password should be minimum 6 characters";
        }

        if (isTextEmpty(confPasswordEdt)) {
            return "Enter confirm password";
        }

        if (!getText(passwordEdt).equals(getText(confPasswordEdt))) {
            return "Password and confirm password are not matching";
        }

        if (isTextEmpty(addressEdt)) {
            return "Enter Address";
        }

        /*if (isTextEmpty(uidEdt)) {
            return "Enter UID";
        }*/

        if (isTextEmpty(dobEdt)) {
            return "Select Date of birth";
        }

        /*if (isTextEmpty(locationEdt)) {
            return "Enter your location";
        }*/

        if(!(imgFilePath != null && !imgFilePath.trim().isEmpty())){
            return "Please capture profile photo";
        }

        return null;
    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    /*private static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }*/

    private void navigateApp() {
        Intent intent = new Intent(RegistrationActivity.this, OTPActivity.class);
        intent.putExtra("email", getText(emailIdEdt));
        finish();
        startActivity(intent);
    }

    private boolean isTextEmpty(EditText editText) {
        if (!editText.getText().toString().trim().isEmpty())
            return false;
        else
            return true;
    }

    private String getText(EditText editText) {
        return editText.getText().toString().trim();
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API).build();
            createLocationRequest();
        }
    }

    /**
     * Creating location request object
     */
    protected void createLocationRequest() {
        if (mLocationRequest == null) {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(0);
            mLocationRequest.setFastestInterval(0);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setSmallestDisplacement(0); // 10 meters
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());

    }

    @Override
    public void onConnected(Bundle arg0) {
        if (location != null && location.length() > 0)
            return;
        try {
            Location loc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (loc != null) {
                location = "" + loc.getLatitude() + "," + loc.getLongitude();
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }

        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            startLocationUpdates();
            mLocationCountDownTimer = new CountDownTimer(COUNT_DOWN_TIME, COUNT_DOWN_TIME_INTERVAL) {

                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    try {
                        stopLocationUpdates();
                        Location loc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        if (loc != null) {
                            location = "" + loc.getLatitude() + "," + loc.getLongitude();
                        }
                    } catch (SecurityException e) {

                    }
                }
            };
            mLocationCountDownTimer.start();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();

    }

    /**
     * Starting the location updates
     */
    protected void startLocationUpdates() {
        try {
            if (mGoogleApiClient.isConnected())
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } catch (SecurityException e) {

        }

    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onLocationChanged(Location loc) {
        // Assign the new location
        //mLastLocation = location;

        if (loc != null) {
            location = "" + loc.getLatitude() + "," + loc.getLongitude();
            stopLocationUpdates();
            cancelLocationTimer();
        }

    }

    private void cancelLocationTimer() {
        if (mLocationCountDownTimer != null) {
            mLocationCountDownTimer.cancel();
            mLocationCountDownTimer = null;
        }
    }


    protected void getCurrentLocation(final Context context) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (mGoogleApiClient != null) {
                if (mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.disconnect();
                }
                mGoogleApiClient.connect();
            }
            showProgressDialog();
            new CountDownTimer(COUNT_DOWN_TIME, COUNT_DOWN_TIME_INTERVAL) {

                @Override
                public void onTick(long millisUntilFinished) {
                    if (location != null) {
                        hideProgressDialog();
                        Toast.makeText(context, MyConstants.GOT_LOCATION_MSG, Toast.LENGTH_SHORT).show();
                        this.cancel();
                    }
                }

                @Override
                public void onFinish() {
                    hideProgressDialog();
                    Toast.makeText(context, MyConstants.GOT_LOCATION_MSG, Toast.LENGTH_SHORT).show();
                    checkForLocation = false;
                    this.cancel();
                }
            }.start();
        } else {
            Utility.showAlertToOpenGPS(context);
        }

    }

    public void showProgressDialog() {
        dialog = new ProgressDialog(context);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    public void hideProgressDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public void onStart() {
        super.onStart();
        if ((mGoogleApiClient != null) && (!mGoogleApiClient.isConnected())) {
            mGoogleApiClient.connect();
        }
    }

    public void onStop() {
        super.onStop();
        if ((mGoogleApiClient != null) && (mGoogleApiClient.isConnected())) {
            mGoogleApiClient.disconnect();
        }
    }

    private void dispatchTakePictureIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, IMAGECAPTURE_ACTIVITY);
        /*Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, IMAGECAPTURE_ACTIVITY);
        }*/
    }

    private void captureImage() {
        File dir;
        if (Utility.getFileDir(context) != null && Utility.getFileDir(context).length() > 0) {
            dir = new File(Utility.getFileDir(context));
        } else {

//            File sdCard = Environment.getExternalStorageDirectory();
            PackageManager m = getPackageManager();
            String s = getPackageName();
            try {
                PackageInfo p = m.getPackageInfo(s, 0);
                s = p.applicationInfo.dataDir;
            } catch (PackageManager.NameNotFoundException e) {
                Log.w("yourtag", "Error Package name not found ", e);
                return;
            }

            File sdCard = new File(s);
            dir = new File(sdCard.getAbsolutePath() + "/iSave");

            if (!dir.exists()) {
                dir.mkdir();
            }

            Utility.setFileDir(dir.toString(), context);
        }


        String uniqueId = getText(emailIdEdt)+".png";
        current = uniqueId;
        mypath = new File(dir, current);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        uriSavedImage = Uri.fromFile(mypath);
        //intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);

        if (hasImageCaptureBug()) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        } else {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        }

        startActivityForResult(intent, IMAGECAPTURE_ACTIVITY);
    }

    public boolean hasImageCaptureBug() {

        // list of known devices that have the bug
        ArrayList<String> devices = new ArrayList<String>();
        devices.add("android-devphone1/dream_devphone/dream");
        devices.add("generic/sdk/generic");
        devices.add("vodafone/vfpioneer/sapphire");
        devices.add("tmobile/kila/dream");
        devices.add("verizon/voles/sholes");
        devices.add("google_ion/google_ion/sapphire");

        return devices.contains(android.os.Build.BRAND + "/" + android.os.Build.PRODUCT + "/"
                + android.os.Build.DEVICE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case IMAGECAPTURE_ACTIVITY:
                if (requestCode == IMAGECAPTURE_ACTIVITY && resultCode == Activity.RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(photo);
                    mCameraImg.setImageDrawable(bitmapDrawable);

                    try {
                        File dir;
                        if (Utility.getFileDir(RegistrationActivity.this) != null && Utility.getFileDir(RegistrationActivity.this).length() > 0) {
                            dir = new File(Utility.getFileDir(RegistrationActivity.this));
                        } else {

                            PackageManager m = getPackageManager();
                            String s = getPackageName();
                            try {
                                PackageInfo p = m.getPackageInfo(s, 0);
                                s = p.applicationInfo.dataDir;
                            } catch (PackageManager.NameNotFoundException e) {
                                Log.w("yourtag", "Error Package name not found ", e);
                                return;
                            }

                            File sdCard = new File(s);
                            dir = new File(sdCard.getAbsolutePath() + "/iSave");

                            if (!dir.exists()) {
                                dir.mkdir();
                            }

                            Utility.setFileDir(dir.toString(), RegistrationActivity.this);
                        } // boolean
                        //status = isExternalStoragePresent();

                        String uniqueId = Utility.getCurrentDate().replaceAll("/", "_") + "_" + System.currentTimeMillis() + ".png";
                        current = uniqueId;
                        System.out.println("current" + current);
                        mypath = new File(dir, current);
                        FileOutputStream mFileOutStream = new FileOutputStream(mypath);
                        photo.compress(Bitmap.CompressFormat.PNG, MyConstants.photoQuality, mFileOutStream); // Converting to byte array
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        photo.compress(Bitmap.CompressFormat.JPEG, MyConstants.photoQuality, stream);
                        imgFilePath = current;
                        Utility.setProfileImageURL(current, RegistrationActivity.this);
                        System.out.println("capture image  ===" + mypath);
                        mFileOutStream.flush();
                        mFileOutStream.close();

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    /*try {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 4;
                        final Bitmap bitmap = BitmapFactory.decodeFile(
                                uriSavedImage.getPath(), options);
                        FileOutputStream mFileOutStream;
                        try {
                            mFileOutStream = new FileOutputStream(
                                    uriSavedImage.getPath());
                            bitmap.compress(Bitmap.CompressFormat.PNG, 0, mFileOutStream);
                            mFileOutStream.flush();
                            mFileOutStream.close();
                            imgFilePath = current;
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        mCameraImg.setImageDrawable(new BitmapDrawable(bitmap));
                        //pending
                        //mCamera.setBackgroundDrawable(new BitmapDrawable(bitmap));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
                }
                break;
        }
    }

    public static String getIMEINo(Context context){
        try {
            StringBuilder builder = new StringBuilder("");
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT >= 23) {
                builder.append(manager.getDeviceId(0));
                if (manager.getPhoneCount() == 2) {
                    builder.append(";");
                    builder.append(manager.getDeviceId(1));
                }
            }
            return builder.toString();
        }catch (Exception e){
            return null;
        }
    }


}





