package com.example.maheshraja.mycampus.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;

import java.util.HashMap;
import java.util.Stack;

//import com.cands.techgenius.mysafe.vizag.R;
//import com.cands.techgenius.utility.MyConstants;
//import com.splunk.mint.Mint;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {
    private Context context;
    private static String TAG = MainActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;


    /* A HashMap of stacks, where we use tab identifier as keys.. */
    public HashMap<String, Stack<BaseFragment>> mStacks;
    private String mCurrentTab;

    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context=this;

        //  Mint.initAndStartSession(MainActivity.this, "89a68d2b");

   mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);

        actionBar = getSupportActionBar();

        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

       BitmapDrawable background = new BitmapDrawable (BitmapFactory.decodeResource(getResources(), R.drawable.toolbarfinal2_logo));
        actionBar.setBackgroundDrawable(background);

        mStacks = new HashMap<String, Stack<BaseFragment>>();
        mStacks.put(MyConstants.TAB_HOME, new Stack<BaseFragment>());
        mStacks.put(MyConstants.TAB_EVENTS, new Stack<BaseFragment>());
        mStacks.put(MyConstants.TAB_Coupons, new Stack<BaseFragment>());
        mStacks.put(MyConstants.TAB_SPONSERS, new Stack<BaseFragment>());
        mStacks.put(MyConstants.TAB_SPONSERS, new Stack<BaseFragment>());
       // mStacks.put(MyConstants.TAB_DASHBOARD, new Stack<BaseFragment>());

        mStacks.put(MyConstants.TAB_HELP, new Stack<BaseFragment>());
        mStacks.put(MyConstants.TAB_SETTINGS, new Stack<BaseFragment>());

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);

        // display the first navigation drawer view on app launch
        displayView(0);
    }


    @Override
    public void onDrawerItemSelected(View view, int position) {
        drawerFragment.updateProfilePic();
        displayView(position);
    }

    private void displayView(int position) {
        BaseFragment fragment = null;
        String title = getString(R.string.app_name);

        switch (position) {
            case 0:
                mCurrentTab = MyConstants.TAB_HOME;
                mStacks.get(mCurrentTab).clear();
                fragment = new AboutApp2Fragment();
                pushFragments(mCurrentTab, fragment, false, true);
                title = getString(R.string.title_home);
                break;
            case 1:
                mCurrentTab = MyConstants.TAB_EVENTS;
                mStacks.get(mCurrentTab).clear();
                fragment = new EventsFragment();
                pushFragments(mCurrentTab, fragment, false, true);
                title = getString(R.string.title_event);
                break;
            case 2:
                mCurrentTab = MyConstants.TAB_Coupons;
                mStacks.get(mCurrentTab).clear();
                fragment = new CouponsFragment();
                pushFragments(mCurrentTab, fragment, false, true);
                title = getString(R.string.title_coupons);
                break;
            case 3:
                mCurrentTab = MyConstants.TAB_SPONSERS;
                mStacks.get(mCurrentTab).clear();
                fragment = new SponsorsFragment();
                pushFragments(mCurrentTab, fragment, false, true);
                title = getString(R.string.title_sponsors);
                break;

          /*  case 4:
                mCurrentTab = MyConstants.TAB_DASHBOARD;
                mStacks.get(mCurrentTab).clear();
                fragment = new DashboardFragment();
                pushFragments(mCurrentTab, fragment, false, true);
                title = getString(R.string.title_dashboard);
                break;*/


            case 4:

                try {
                    mCurrentTab = MyConstants.TAB_DASHBOARD;
                    //mStacks.get(mCurrentTab).clear();
                    Utility.showProgressDialog(context);
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);

                    context.deleteDatabase("security.db");

                    startActivity(intent);
                    title = getString(R.string.title_logout);
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                }


        }


        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

        }
    }

    public void pushFragments(String tag, BaseFragment fragment, boolean shouldAnimate, boolean shouldAdd) {
        if (shouldAdd)
            mStacks.get(tag).push(fragment);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();

        ft.replace(R.id.container_body, fragment);

        ft.commit();

    }

    public void popFragments() {
		/*
		 * Select the second last fragment in current tab's stack.. which will
		 * be shown after the fragment transaction given below
		 */
        Fragment fragment = mStacks.get(mCurrentTab).elementAt(
                mStacks.get(mCurrentTab).size() - 2);

		/* pop current fragment from stack.. */
        mStacks.get(mCurrentTab).pop();

		/*
		 * We have the target fragment in hand.. Just show it.. Show a standard
		 * navigation animation
		 */
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        // ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        ft.replace(R.id.container_body, fragment);
        ft.commit();
    }
    public void onBackPressed() {
        boolean temp = true;
        if (temp) {
            temp = false;
            /*if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            }*/



					/*
					 * top fragment in current tab doesn't handles back press,
					 * we can do our thing, which is
					 *
					 * if current tab has only one fragment in stack, ie first
					 * fragment is showing for this tab. finish the activity
					 * else pop to previous fragment in stack for the same tab
					 */
            if (mStacks.get(mCurrentTab).size() == 1) {
                System.out.println("mStacks is:"+mStacks);
                System.out.println("mCurrentTab is:"+mCurrentTab);
                if (mCurrentTab.equals(MyConstants.TAB_HOME)) {
                /*    System.out.println("swarupa is:");
                    int stackSize = mStacks.get(mCurrentTab).size();
                    System.out.println("stackSize is:"+stackSize);
                    BaseFragment fragment = mStacks.get(mCurrentTab).get(stackSize-1);
                    popFragments();*/
                    alertDialog();
                } else {
                    displayView(0);
                    System.out.println("swarupa1 is:");
//							if(fragment.onBackPressed())
//								displayView(10);
                }
						/*
						 * if (trackingButtonTextStatus.equals("STOP TRACKING"))
						 * { } else {
						 */
                // super.onBackPressed(); // or call finish..
                // alertDialog();
                // }
            } else {
               int stackSize = mStacks.get(mCurrentTab).size();
                System.out.println("stackSize is:"+stackSize);
                BaseFragment fragment = mStacks.get(mCurrentTab).get(stackSize-1);
                System.out.println("fragment"+fragment);
                popFragments();
                //if(fragment.onBackPressed())
							//popFragments();
            }
        } else {
            // do nothing.. fragment already handled back button press.
        }
        temp = true;

    }



    private void alertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exit");
        builder.setMessage(getResources().getString(R.string.exitMsg))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                finish();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void updateProfilePic(){
        drawerFragment.updateProfilePic();
    }
}