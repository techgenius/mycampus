package com.example.maheshraja.mycampus.dtos;

/**
 * Created by swamy on 29-05-2016.
 */
public class LoginDataDTO implements DTO {

    private UserDTO user;


    public JobPortalDTO getSeekerdetails() {
        return seekerdetails;
    }

    public void setSeekerdetails(JobPortalDTO seekerdetails) {
        this.seekerdetails = seekerdetails;
    }

    private JobPortalDTO seekerdetails;
    private int code;
    private String profilePicUrl;

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

}
