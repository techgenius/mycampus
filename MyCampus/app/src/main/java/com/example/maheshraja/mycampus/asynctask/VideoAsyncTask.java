package com.example.maheshraja.mycampus.asynctask;

/**
 * Created by Swamy on 3/4/2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.example.maheshraja.mycampus.daos.UserDAO;
import com.example.maheshraja.mycampus.database.DBHandler;
import com.example.maheshraja.mycampus.dtos.UserDTO;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Yakaswamy.g on 2/22/2016.
 */

public class VideoAsyncTask extends AsyncTask<String, Void, String> {

    private String jsonData;
    private AsyncTaskCompleteListener asyncTaskCompleteListener;
    private int connectionTimeout;
    private int socketTimeout;
    private String imageUrl;
    private Context context = null;
    public VideoAsyncTask(Context context,String imageUrl, AsyncTaskCompleteListener asyncTaskCompleteListener, int connectionTimeout, int socketTimeout) {
        this.imageUrl = imageUrl;
        this.context = context;
        Utility.showAlert(context, null, "imageUrl to: " +
                imageUrl);
        this.asyncTaskCompleteListener = asyncTaskCompleteListener;
        this.connectionTimeout = connectionTimeout;
        this.socketTimeout = socketTimeout;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {

        String charset = "UTF-8";

        String requestURL = MyConstants.VIDEO_APP_URL + params[0];
        if(imageUrl!=null && imageUrl.length()>7)
            imageUrl = imageUrl.substring(7);

        System.out.println("requestURL=" + requestURL + ">>>>> with imageUrl="+imageUrl);


        File uploadFile1 = new File(imageUrl);


        //File uploadFile2 = new File("e:/Test/PIC2.JPG");

//        Utility.showAlert(context, null, "requestURL to upload  : " +
//                requestURL);
        System.out.println("requestURL=" + requestURL + " with uploadFile1="+imageUrl);

        try {
            MultipartUtility multipart = new MultipartUtility(requestURL, charset);

            multipart.addHeaderField("User-Agent", "CodeJava");
            multipart.addHeaderField("Test-Header", "Header-Value");

            UserDTO user = (UserDTO) UserDAO.getInstance().getRecords(DBHandler.getInstance(context).getDBObject(0)).get(0);

            multipart.addFormField("userid", user.getEmailId());
            multipart.addFormField("password", user.getPassword());
            UserDTO localObject = (UserDTO) UserDAO.getInstance().getRecords(DBHandler.getInstance(context).getDBObject(0)).get(0);
            String str = "" + localObject.getName();
            str = str + "/" + localObject.getAge();
            str = str + "" + localObject.getGender().charAt(0);
            str = str + "\nMNo:" + localObject.getMobileNo();

            multipart.addFormField("message", str);

            multipart.addFilePart("fileUpload", uploadFile1);
            //multipart.addFilePart("fileUpload", uploadFile2);

            List<String> response = multipart.finish();

            System.out.println("SERVER REPLIED:");

            return "{\"code\":100}";
        } catch (IOException ex) {
            System.err.println(ex);
            ex.printStackTrace();;
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        asyncTaskCompleteListener.onAsynComplete(result);
    }
}


