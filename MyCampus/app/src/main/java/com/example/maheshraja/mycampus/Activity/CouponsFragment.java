package com.example.maheshraja.mycampus.Activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.maheshraja.mycampus.R;


/**
 * Created by Swamy on 3/5/2016.
 */
public class CouponsFragment extends BaseFragment {

    TextView text;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_coupons, container, false);

        text = (TextView) rootView.findViewById(R.id.txttest);
        return rootView;
    }
}