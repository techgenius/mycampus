package com.example.maheshraja.mycampus.dtos;

import java.util.List;

/**
 * Created by MaheshRaja on 6/28/2016.
 */
public class AllSponserDTO {

    List<SponserTypeDTO> type;

    public List<SponserTypeDTO> getType() {
        return type;
    }

    public void setType(List<SponserTypeDTO> type) {
        this.type = type;
    }
}
