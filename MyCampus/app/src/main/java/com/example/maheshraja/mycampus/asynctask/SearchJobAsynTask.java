package com.example.maheshraja.mycampus.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by lenovo on 29-07-2016.
 */
public class SearchJobAsynTask extends AsyncTask<String, Void, String> {

    private String jsonData;
    private AsyncTaskCompleteListener asyncTaskCompleteListener;
    private int connectionTimeout;
    private int socketTimeout;

    public SearchJobAsynTask(String requestData, AsyncTaskCompleteListener asyncTaskCompleteListener, int connectionTimeout, int socketTimeout) {
        jsonData = requestData;
        this.asyncTaskCompleteListener = asyncTaskCompleteListener;
        this.connectionTimeout = connectionTimeout;
        this.socketTimeout = socketTimeout;
    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        HttpURLConnection conn = null;
        try {
            URL myurl = new URL(MyConstants.APP_URL+params[0]);
            System.out.println("Url is:"+myurl);
            conn = (HttpURLConnection) myurl.openConnection();
            conn.setConnectTimeout(connectionTimeout);
            conn.setReadTimeout(socketTimeout);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStream os = conn.getOutputStream();
            os.write(jsonData.getBytes());
            os.flush();

            InputStream inputStream = conn.getInputStream();
            String responseStr = Utility.convertStreamToString(inputStream);
            Log.i("Async", responseStr);
            return responseStr;
        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            if (conn != null)
                conn.disconnect();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result1) {
        asyncTaskCompleteListener.onAsynComplete(result1);
    }
}
