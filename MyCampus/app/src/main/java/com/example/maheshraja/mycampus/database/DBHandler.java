package com.example.maheshraja.mycampus.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Swamy on 3/2/2016.
 */
public class DBHandler extends SQLiteOpenHelper {

    private final String TAG ="DBHandler";
    private static final String DATABASE_NAME="security.db";
    public static final int DATABASE_VERSION =1;
    private static Context context;
    private static  DBHandler dbHandler;



    public static DBHandler getInstance(Context context2){

        if(dbHandler == null){
            context = context2;
            dbHandler =new DBHandler(context2);
        }

        return  dbHandler;
    }






    public SQLiteDatabase getDBObject(int isWritable){
        return (isWritable == 1)? this. getWritableDatabase():this.getReadableDatabase();

    }



    /**
     * Constructor
     *
     * @param context
     *            :Interface to global information about an application
     *            environment.
     */
    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should
     * happen.
     *
     * @param db
     *            : The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        /*USER
        id
        name
        mobileNo
        email
        gender
        address
        uid
        uidType
        dob
        location
        */

        db.execSQL("CREATE TABLE USER(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, mobileNo TEXT, email TEXT, gender TEXT, address TEXT, uid TEXT, uidType TEXT, dob TEXT, location TEXT, password TEXT)");


        db.execSQL("CREATE TABLE FRIENDS_CNT(id INTEGER PRIMARY KEY AUTOINCREMENT,userName TEXT, mobileNumber TEXT, preferedLocation TEXT, emailAddress TEXT, resumeHeadline TEXT, keySkills TEXT, SSCPercentage TEXT, interPercentage TEXT, btechPercentage TEXT)");

                /*APP_NAVIGATION
                  friends
                  forcePage
                */
        //System.out.println(db.execSQL("CREATE TABLE FRIENDS_CNT(id INTEGER PRIMARY KEY AUTOINCREMENT,userName TEXT, mobileNumber TEXT, preferedLocation TEXT, emailAddress TEXT, resumeHeadline TEXT, keySkills TEXT, SSCPercentage TEXT, interPercentage TEXT, btechPercentage TEXT)"));
        db.execSQL("CREATE TABLE APP_NAVIGATION(friends NUMBER, forcePage NUMBER)");


    }
    /**
     * Called when the database needs to be upgraded.
     *
     * @param db
     *            : The database.
     * @param oldVersion
     *            : The old database version.
     * @param newVersion
     *            : The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
