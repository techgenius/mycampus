package com.example.maheshraja.mycampus.Activity;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.asynctask.AudioAsyncTask;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class AudioRecordActivity extends Activity {

    private static final String LOG_TAG = "AudioRecordActivity";
    private static String mFileName = null;
    private Context context;

    private RecordButton mRecordButton = null;
    private MediaRecorder mRecorder = null;

    private PlayButton   mPlayButton = null;
    private MediaPlayer   mPlayer = null;


    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_audio_capture);
        context = this;
        LinearLayout ll = (LinearLayout) findViewById(R.id.audio_rec);
        mRecordButton = new RecordButton(this);
        ll.addView(mRecordButton,
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,
                        0));
        mPlayButton = new PlayButton(this);
       /* ll.addView(mPlayButton,
                new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        0));*/
        //setContentView(ll);
    }


    private void onRecord(boolean start) {
        if (start) {
            startRecording();
        } else {
            stopRecording();
        }
    }

    private void onPlay(boolean start) {
        if (start) {
            startPlaying();
        } else {
            stopPlaying();
        }
    }

    private void startPlaying() {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(mFileName);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;

    }

    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        mRecorder.start();
    }

    private void stopRecording() {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        if(mFileName!=null) {

            AudioAsyncTask imageAsyncTask = new AudioAsyncTask(context, mFileName, taskCompleteListener, MyConstants.IMG_CONNECTION_TIMEOUT, MyConstants.IMG_SOCKET_TIMEOUT);
            Utility.showProgressDialog(context);
            imageAsyncTask.execute(MyConstants.AUDIO_UPLOAD_SERVICE);

            Toast.makeText(this, "stopRecording is:" + mFileName, Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(this, "stopRecording is:" + mFileName, Toast.LENGTH_LONG).show();
        }

    }

    class RecordButton extends Button {
        boolean mStartRecording = true;

        OnClickListener clicker = new OnClickListener() {
            public void onClick(View v) {
                onRecord(mStartRecording);
                if (mStartRecording) {
                    setText("Stop recording");




                } else {
                    setText("Start recording");


                }
                mStartRecording = !mStartRecording;
            }
        };

        public RecordButton(Context ctx) {
            super(ctx);
            setText("Start recording");
            setOnClickListener(clicker);

        }
    }

    class PlayButton extends Button {
        boolean mStartPlaying = true;

        OnClickListener clicker = new OnClickListener() {
            public void onClick(View v) {
                onPlay(mStartPlaying);
               /* if (mStartPlaying) {
                    setText("Stop playing");
                }
                mStartPlaying = !mStartPlaying;*/
            }
        };

        public PlayButton(Context ctx) {
            super(ctx);
           /* setText("Start playing");
            setOnClickListener(clicker);*/
        }
    }
    java.util.Date date= new java.util.Date();
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
            .format(date.getTime());
    static String  fileName;

    public AudioRecordActivity() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MySafeAudio");
       /* mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFileName += "/audiorecordtest"+timeStamp+".3gp";*/
        if(!mediaStorageDir.exists()){
            mediaStorageDir.mkdirs();
        }

        mFileName = mediaStorageDir.getPath() + File.separator +
                "AUD_"+ timeStamp + ".3gp";

     /*   Toast.makeText(this, "AudioRecordActivity will update in :" + mFileName, Toast.LENGTH_LONG).show();
*/
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }

        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }

    private AsyncTaskCompleteListener taskCompleteListener = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result) {
            if (result != null) {
                if(Utility.isJSONValid(result)){
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.optInt("code");
                        if(code == 100){
                            //navigateApp();
                            Utility.showToast(context, "Audio Recorded and Uploaded Successfully to Police Control Room");
                        }else if(code == 101){
                            Utility.showToast(context, "Audio Recorded But Failed to Upload to Police Control Room");
                        }else{
                            Utility.showToast(context, "Unexpected Error Please Contact MySafe Team @ www.mysafewithu.org");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Utility.showToast(context, "Invalid Response contact system admin");
                }

            } else {
                Utility.showToast(context, "Check your internet connection");
            }
            Utility.hideProgressDialog();
            finish();
        }
    };
}