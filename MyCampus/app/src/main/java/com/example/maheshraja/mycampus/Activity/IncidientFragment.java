package com.example.maheshraja.mycampus.Activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.asynctask.MyAsyncTask;
import com.example.maheshraja.mycampus.daos.UserDAO;
import com.example.maheshraja.mycampus.database.DBHandler;
import com.example.maheshraja.mycampus.dtos.DTO;
import com.example.maheshraja.mycampus.dtos.UserDTO;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.DistanceCalculationManager;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class IncidientFragment extends BaseFragment {

    private RadioGroup surakshaRadioGroup;
    private EditText edtMsg;
    private Button btnSubmit;
    private ImageView btnCall;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_incidient, container, false);

        surakshaRadioGroup = (RadioGroup) rootView.findViewById(R.id.suraksha_radio_group);
        edtMsg = (EditText) rootView.findViewById(R.id.message);
        btnSubmit = (Button) rootView.findViewById(R.id.submit);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtMsg.getText().toString().trim().length() > 0) {
                    sendData();
                } else {
                    Utility.showAlert(mActivity, null, "Please enter your message");
                }
            }
        });

        return rootView;
    }

    private void sendData() {
        String url = null;
        String msg = null;
        switch (surakshaRadioGroup.getCheckedRadioButtonId()) {

            case R.id.radioChain:
                url = MyConstants.CHAIN_URL;
                msg = getMessageForChain(edtMsg.getText().toString().trim());
                break;
            case R.id.radioOther:
                url = MyConstants.OTHER_URL;
                msg = getMessageForOther(edtMsg.getText().toString().trim());
                break;
        }

        if (url != null) {
            if (location != null) {

                DistanceCalculationManager distMngr = new DistanceCalculationManager();

                MultipleSMS(distMngr.getSHOCellNumber(distMngr.getNearestSHO(Double.valueOf(location.split(",")[0]), Double.valueOf(location.split(",")[1]))), msg);

                if (Utility.networkAvailability(mActivity)) {
                    String reqData = getRequestData(edtMsg.getText().toString().trim());
                    MyAsyncTask asyncTask = new MyAsyncTask(reqData, onAsyncComplete, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
                    Utility.showProgressDialog(mActivity);
                    asyncTask.execute(url);
                } else {
                    Utility.showAlert(mActivity, null, "Please check your network connection");
                }
            } else {
                getCurrentLocation(mActivity);
            }
        } else {
            Utility.showAlert(mActivity, null, "Select your complaint type");
        }
    }

    private void MultipleSMS(final String phoneNumber, final String message) {

        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(mActivity, 0, new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(mActivity, 0, new Intent(DELIVERED), 0);


// For when the SMS has been sent
        mActivity.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        /*ContentValues values = new ContentValues();
                            for (int i = 0; i < MobNumber.size() - 1; i++) {
                                values.put("address", MobNumber.get(i).toString());
                                // txtPhoneNo.getText().toString());
                                values.put("body", message);
                            }
                            mActivity.getContentResolver().insert(Uri.parse("content://sms/sent"), values);*/
                        Toast.makeText(context, "SMS sent successfully", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(context, "Generic failure cause", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(context, "Service is currently unavailable", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(context, "No pdu provided", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(context, "Radio was explicitly turned off", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

// For when the SMS has been delivered
        mActivity.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(mActivity.getBaseContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(mActivity.getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }

    public void onStart() {
        super.onStart();
        if ((mGoogleApiClient != null) && (!mGoogleApiClient.isConnected())) {
            mGoogleApiClient.connect();
        }
    }

    public void onStop() {
        super.onStop();
        if ((mGoogleApiClient != null) && (mGoogleApiClient.isConnected())) {
            mGoogleApiClient.disconnect();
        }
    }

    private String getRequestData(String msg) {
        String userId;
        List<DTO> users = UserDAO.getInstance().getRecords(DBHandler.getInstance(mActivity).getDBObject(0));
        if (users == null || users.size() == 0)
            userId = "";
        else {
            UserDTO user = (UserDTO) users.get(0);
            userId = user.getEmailId();
        }
        JSONObject object = new JSONObject();
        try {
            object.put("deviceId", Utility.getDeviceId(mActivity));
            object.put("latLong", location);

            UserDTO localObject = (UserDTO) UserDAO.getInstance().getRecords(DBHandler.getInstance(this.mActivity).getDBObject(0)).get(0);
            String str = "" + localObject.getName();
            str = str + "/" + localObject.getAge();
            str = str + "" + localObject.getGender().charAt(0);
            str = str + "\nMNo:" + localObject.getMobileNo();

            object.put("message", str + "##" + msg);
            DistanceCalculationManager distMngr = new DistanceCalculationManager();
            object.put("shoCell", distMngr.getSHOCellNumber(distMngr.getNearestSHO(Double.valueOf(location.split(",")[0]), Double.valueOf(location.split(",")[1]))));
            object.put("userId", userId);
            object.put("authcode", Utility.getAuthCode(mActivity));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return object.toString();
    }

    private AsyncTaskCompleteListener onAsyncComplete = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result) {
            if (result != null) {
                if (Utility.isJSONValid(result)) {
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.optInt("code");
                        if (code == 100) {
                            clearForm();
                            Utility.showAlert(mActivity, null, "Informed Successfully");
                        } else if (code == 101) {
                            Utility.showAlert(mActivity, null, "Failed Informed");
                        } else {
                            Utility.showAlert(mActivity, null, "Unknown response");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utility.showAlert(mActivity, null, "Invalid Response contact system admin");
                }

            } else {
                Utility.showAlert(mActivity, "Network problem", "Check your internet connection");
            }
            Utility.hideProgressDialog();
        }
    };

    private void clearForm() {
        edtMsg.setText("");
        surakshaRadioGroup.clearCheck();
    }

    private String getMessageForRagging(String msg) {
        UserDTO localObject = (UserDTO) UserDAO.getInstance().getRecords(DBHandler.getInstance(this.mActivity).getDBObject(0)).get(0);
        String str = "" + localObject.getName();
        str = str + "/" + localObject.getAge();
        str = str + "" + localObject.getGender().charAt(0);
        str = str + "/MNo:" + localObject.getMobileNo();
        str = str + "\nis looking for Suraksha from ragging at " + getMapUrl(location);
        if (msg != null && !msg.isEmpty())
            str = str + "\nUSRMSG:" + msg;
        return str;
    }

    private String getMessageForDomestic(String msg) {
        UserDTO localObject = (UserDTO) UserDAO.getInstance().getRecords(DBHandler.getInstance(this.mActivity).getDBObject(0)).get(0);
        String str = "" + localObject.getName();
        str = str + "/" + localObject.getAge();
        str = str + "" + localObject.getGender().charAt(0);
        str = str + "/MNo:" + localObject.getMobileNo();
        str = str + "\nis looking for Suraksha from Domestic violence at " + getMapUrl(location);
        if (msg != null && !msg.isEmpty())
            str = str + "\nUSRMSG:" + msg;
        return str;
    }

    private String getMessageForSexual(String msg) {
        UserDTO localObject = (UserDTO) UserDAO.getInstance().getRecords(DBHandler.getInstance(this.mActivity).getDBObject(0)).get(0);
        String str = "" + localObject.getName();
        str = str + "/" + localObject.getAge();
        str = str + "" + localObject.getGender().charAt(0);
        str = str + "/MNo:" + localObject.getMobileNo();
        str = str + "\nis looking for Suraksha from Sexual harassment at " + getMapUrl(location);
        if (msg != null && !msg.isEmpty())
            str = str + "\nUSRMSG:" + msg;
        return str;
    }

    private String getMessageForChain(String msg) {
        UserDTO localObject = (UserDTO) UserDAO.getInstance().getRecords(DBHandler.getInstance(this.mActivity).getDBObject(0)).get(0);
        String str = "" + localObject.getName();
        str = str + "/" + localObject.getAge();
        str = str + "" + localObject.getGender().charAt(0);
        str = str + "/MNo:" + localObject.getMobileNo();
        str = str + "\nis looking for Suraksha from chain snatching at " + getMapUrl(location);
        if (msg != null && !msg.isEmpty())
            str = str + "\nUSRMSG:" + msg;
        return str;
    }

    private String getMessageForOther(String msg) {
        UserDTO localObject = (UserDTO) UserDAO.getInstance().getRecords(DBHandler.getInstance(this.mActivity).getDBObject(0)).get(0);
        String str = "" + localObject.getName();
        str = str + "/" + localObject.getAge();
        str = str + "" + localObject.getGender().charAt(0);
        str = str + "/MNo:" + localObject.getMobileNo();
        str = str + "\nis looking for Suraksha from other issue at " + getMapUrl(location);
        if (msg != null && !msg.isEmpty())
            str = str + "\nUSRMSG:" + msg;
        return str;
    }

    private String getMapUrl(String paramString) {
        return "https://www.google.co.in/maps/place//@" + paramString + ",16z";
    }
}
