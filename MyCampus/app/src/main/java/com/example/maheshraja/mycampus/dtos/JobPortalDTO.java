package com.example.maheshraja.mycampus.dtos;

import java.io.Serializable;

/**
 * Created by lenovo on 27-07-2016.
 */
public class JobPortalDTO implements DTO,Serializable {
    public String getKeyword() {
        return keyword;
    }

    public String getCampus_emailid() {
        return campus_emailid;
    }

    public void setCampus_emailid(String campus_emailid) {
        this.campus_emailid = campus_emailid;
    }

    String campus_emailid;

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    String keyword;

    public String getWanted_exp() {
        return wanted_exp;
    }

    public void setWanted_exp(String wanted_exp) {
        this.wanted_exp = wanted_exp;
    }

    public String getInterview_loc() {
        return interview_loc;
    }

    public void setInterview_loc(String interview_loc) {
        this.interview_loc = interview_loc;
    }

    String wanted_exp;
    String interview_loc;
    String tagline;

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    String  company_name;

    public String getCompany_loc() {
        return company_loc;
    }

    public void setCompany_loc(String company_loc) {
        this.company_loc = company_loc;
    }

    String company_loc;

    public String getJobdescription() {
        return Jobdescription;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    String jobId;

    public void setJobdescription(String jobdescription) {
        Jobdescription = jobdescription;
    }

    String Jobdescription;

    String userName,mobileNumber, preferedLocation, emailAddress, resumeHeadline, keySkills;


    String SSCPercentage;
    String interPercentage;
    String btechPercentage;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getPreferedLocation() {
        return preferedLocation;
    }

    public void setPreferedLocation(String preferedLocation) {
        this.preferedLocation = preferedLocation;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getResumeHeadline() {
        return resumeHeadline;
    }

    public void setResumeHeadline(String resumeHeadline) {
        this.resumeHeadline = resumeHeadline;
    }

    public String getKeySkills() {
        return keySkills;
    }

    public void setKeySkills(String keySkills) {
        this.keySkills = keySkills;
    }

    public String getSSCPercentage() {
        return SSCPercentage;
    }

    public void setSSCPercentage(String sSCPercentage) {
        SSCPercentage = sSCPercentage;
    }

    public String getInterPercentage() {
        return interPercentage;
    }

    public void setInterPercentage(String interPercentage) {
        this.interPercentage = interPercentage;
    }

    public String getBtechPercentage() {
        return btechPercentage;
    }

    public void setBtechPercentage(String btechPercentage) {
        this.btechPercentage = btechPercentage;
    }


    public String toString()
{
    StringBuffer bf=new StringBuffer();
    bf.append("wanted_exp"+wanted_exp);
    bf.append("company_name"+company_name);
    bf.append("company_loc"+company_loc);
    bf.append("tagline"+tagline);
    bf.append("interview_loc"+interview_loc);
    bf.append("keyword"+keyword);

    return bf.toString();
}


}
