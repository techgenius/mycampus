package com.example.maheshraja.mycampus.Activity;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.asynctask.SearchJobAsynTask;
import com.example.maheshraja.mycampus.daos.JobportalDAo;
import com.example.maheshraja.mycampus.database.DBHandler;
import com.example.maheshraja.mycampus.dtos.DTO;
import com.example.maheshraja.mycampus.dtos.JobPortalDTO;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class EditProfileFragment extends BaseFragment {
    private Context context;
    EditText userNameEdt;
    EditText mobileNoEdt;
    EditText emailIdEdt;
    EditText KeySkillEdt;
    EditText reg_resume;
    EditText reg_sscper;
    EditText reg_inter;
    EditText reg_btech;
    Button btnSubmit;
    EditText preferedlocation;
    private Toolbar mToolbar;

    private String TAG = "RegistrationActivity";

    private CountDownTimer mLocationCountDownTimer;
    private ProgressDialog dialog;
    protected final int COUNT_DOWN_TIME = 30000;
    protected final int COUNT_DOWN_TIME_INTERVAL = 1000;
    protected static boolean checkForLocation = true;
    private String current;
    private File mypath;
    private Uri uriSavedImage;
    private final int IMAGECAPTURE_ACTIVITY = 100;
    private String imgFilePath = "";

    private ImageView mCameraImg;

    private View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context=mActivity;

        View rootView = inflater.inflate(R.layout.fragemt_jobportalform, container, false);
        userNameEdt = (EditText)rootView. findViewById(R.id.reg_name);
        mobileNoEdt = (EditText)rootView. findViewById(R.id.reg_mobile);
        emailIdEdt = (EditText)rootView. findViewById(R.id.reg_email);
        KeySkillEdt = (EditText)rootView. findViewById(R.id.reg_keyskills);
        reg_resume = (EditText)rootView. findViewById(R.id.reg_resume);
        reg_sscper = (EditText)rootView. findViewById(R.id.reg_sscper);
        reg_inter=(EditText)rootView. findViewById(R.id.reg_inter);
        reg_btech=(EditText)rootView. findViewById(R.id.reg_btech);
        preferedlocation=(EditText)rootView. findViewById(R.id.reg_location);
        btnSubmit = (Button)rootView. findViewById(R.id.reg_submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String validation = validateFields();
                if (validation == null) {

                    if (Utility.networkAvailability(context)) {
//                        showProgress();
                        registrationProcess();
//                            loginProcess();
                        //saveData(getUserData());;
                    } else {
                        Utility.showAlert(context, null, getString(R.string.check_connection));
                    }

                } else {
                    Utility.showAlert(context, "Alert", validation);
                }
                //uploadImage();
            }
        });



        populateData();

        return rootView;
    }

    private AsyncTaskCompleteListener taskCompleteListener = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result) {
            if (result != null) {
                if(Utility.isJSONValid(result)){
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.optInt("code");
                        if(code == 100){


                            Utility.showAlert(mActivity, null, "Updated Your Profile Successfully");
                        }else if(code == 101){
                            Utility.showAlert(mActivity, null, "Failed Updated Your Profile");
                        }else{
                            Utility.showAlert(mActivity, null, "Unknown response");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Utility.showAlert(mActivity, null, "Invalid Response contact system admin");
                }

            } else {
                Utility.showAlert(mActivity, "Network problem", "Check your internet connection");
            }
            Utility.hideProgressDialog();
        }
    };

    private void registrationProcess() {

        JobPortalDTO dto = (JobPortalDTO) getUserData();

        JSONObject reqJson = new JSONObject();

        try {
            reqJson.put("userName", dto.getUserName());
            System.out.println("userName" + dto.getUserName());
            reqJson.put("mobileNumber", dto.getMobileNumber());
            System.out.println("mobileNumber"+ dto.getMobileNumber());
            reqJson.put("preferedLocation", dto.getPreferedLocation());
            System.out.println("preferedLocation"+ dto.getPreferedLocation());
            reqJson.put("emailAddress", dto.getEmailAddress());
            System.out.println("emailAddress"+dto.getEmailAddress());
            reqJson.put("resumeHeadline", dto.getResumeHeadline());
            System.out.println("resumeHeadline" + dto.getResumeHeadline());
            reqJson.put("keySkills", dto.getKeySkills());
            System.out.println("keySkills" + dto.getKeySkills());
            reqJson.put("SSCPercentage", dto.getSSCPercentage());
            System.out.println("SSCPercentage" + dto.getSSCPercentage());
            reqJson.put("interPercentage", dto.getInterPercentage());
            System.out.println("interPercentage" + dto.getInterPercentage());
            reqJson.put("btechPercentage", dto.getBtechPercentage());
            System.out.println("btechPercentage" + dto.getBtechPercentage());


        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){

        }

        SearchJobAsynTask loginAsync = new SearchJobAsynTask(reqJson.toString(), completeListener, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
        Utility.showProgressDialog(context);
        loginAsync.execute(MyConstants.JobPortal_Update_URL);

    }

    private AsyncTaskCompleteListener completeListener = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result) {
            if (result != null) {
                if(Utility.isJSONValid(result)){
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.optInt("code");
                        if(code == 100){
                            saveData(getUserData());
                            Utility.showAlert(mActivity, null, "Updated Your Profile Successfully");

                            return;
                            //Utility.showAlert(mActivity, null, "Informed Successfully");
                        }else if(code == 101){
                            Utility.showAlert(mActivity, null, "Failed Informed");
                        }else{
                            Utility.showAlert(mActivity, null, "Unknown response");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Utility.showAlert(mActivity, null, "Invalid Response contact system admin");
                }

            } else {
                Utility.showAlert(mActivity, "Network problem", "Check your internet connection");
            }
            Utility.hideProgressDialog();
        }
    };


    private void showProgress() {
        final ProgressDialog progressDialog = new ProgressDialog(mActivity);
        progressDialog.setMessage("Please wait");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        new CountDownTimer(500, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                progressDialog.cancel();
                saveData(getUserData());
            }
        }.start();

    }

    private void saveData(DTO userData) {
        JobportalDAo.getInstance().update(userData, DBHandler.getInstance(mActivity).getDBObject(1));

    }

    private DTO getUserData() {
        JobPortalDTO dto = new JobPortalDTO();
        dto.setUserName(getText(userNameEdt));
        dto.setMobileNumber(getText(mobileNoEdt));
        dto.setEmailAddress(getText(emailIdEdt));
        dto.setPreferedLocation(getText(preferedlocation));
        dto.setResumeHeadline(getText(reg_resume));
        dto.setSSCPercentage(getText(reg_sscper));
        dto.setBtechPercentage(getText(reg_btech));
        dto.setInterPercentage(getText(reg_inter));
        dto.setKeySkills(getText(KeySkillEdt));


        return dto;
    }

    private String validateFields() {
        if (isTextEmpty(userNameEdt)) {
            return "Enter your name";
        }

        if (isTextEmpty(mobileNoEdt)) {
            return "Enter mobile number";
        }

        if (mobileNoEdt.getText().toString().trim().length() <= 9) {
            return "Enter valid mobile number";
        }

        if (isTextEmpty(emailIdEdt)) {
            return "Enter mail ID";
        }

        if(!isValidEmailAddress(getText(emailIdEdt))){
            return "Enter valid mail ID";
        }

        if (isTextEmpty(KeySkillEdt)) {
            return "Enter Keyskill";
        }



        if (isTextEmpty(reg_resume)) {
            return "Enter Resume Headline";
        }



        if (isTextEmpty(reg_sscper)) {
            return "Enter SSC Percentage";
        }

        /*if (isTextEmpty(uidEdt)) {
            return "Enter UID";
        }*/

        if (isTextEmpty(reg_inter)) {
            return "Enter Inter Percentage";
        }

        /*if (isTextEmpty(locationEdt)) {
            return "Enter your location";
        }*/

        if (isTextEmpty(reg_btech)) {
            return "Enter BTECH Percentage";
        }

        if (isTextEmpty(preferedlocation)) {
            return "Enter Loctaion";
        }

        return null;
    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    /*private static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }*/

    private void navigateApp() {
        mActivity.popFragments();
    }

    private boolean isTextEmpty(EditText editText) {
        if (!editText.getText().toString().trim().isEmpty())
            return false;
        else
            return true;
    }

    private String getText(EditText editText) {
        return editText.getText().toString().trim();
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API).build();
            createLocationRequest();
        }
    }

    /**
     * Creating location request object
     */
    protected void createLocationRequest() {
        if (mLocationRequest == null) {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(0);
            mLocationRequest.setFastestInterval(0);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setSmallestDisplacement(0); // 10 meters
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());

    }

    @Override
    public void onConnected(Bundle arg0) {
        if (location != null && location.length() > 0)
            return;
        try {
            Location loc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (loc != null) {
                location = "" + loc.getLatitude() + "," + loc.getLongitude();
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }

        LocationManager lm = (LocationManager) mActivity.getSystemService(mActivity.LOCATION_SERVICE);
        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            startLocationUpdates();
            mLocationCountDownTimer = new CountDownTimer(COUNT_DOWN_TIME, COUNT_DOWN_TIME_INTERVAL) {

                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    try {
                        stopLocationUpdates();
                        Location loc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        if (loc != null) {
                            location = "" + loc.getLatitude() + "," + loc.getLongitude();
                        }
                    } catch (SecurityException e) {

                    }
                }
            };
            mLocationCountDownTimer.start();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();

    }

    /**
     * Starting the location updates
     */
    protected void startLocationUpdates() {
        try {
            if (mGoogleApiClient.isConnected())
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } catch (SecurityException e) {

        }

    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onLocationChanged(Location loc) {
        // Assign the new location
        //mLastLocation = location;

        if (loc != null) {
            location = "" + loc.getLatitude() + "," + loc.getLongitude();
            stopLocationUpdates();
            cancelLocationTimer();
        }

    }

    private void cancelLocationTimer() {
        if (mLocationCountDownTimer != null) {
            mLocationCountDownTimer.cancel();
            mLocationCountDownTimer = null;
        }
    }


    protected void getCurrentLocation(final Context mActivity) {
        LocationManager lm = (LocationManager) mActivity.getSystemService(mActivity.LOCATION_SERVICE);
        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (mGoogleApiClient != null) {
                if (mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.disconnect();
                }
                mGoogleApiClient.connect();
            }
            showProgressDialog();
            new CountDownTimer(COUNT_DOWN_TIME, COUNT_DOWN_TIME_INTERVAL) {

                @Override
                public void onTick(long millisUntilFinished) {
                    if (location != null) {
                        hideProgressDialog();
                        Toast.makeText(mActivity, MyConstants.GOT_LOCATION_MSG, Toast.LENGTH_SHORT).show();
                        this.cancel();
                    }
                }

                @Override
                public void onFinish() {
                    hideProgressDialog();
                    Toast.makeText(mActivity, MyConstants.GOT_LOCATION_MSG, Toast.LENGTH_SHORT).show();
                    checkForLocation = false;
                    this.cancel();
                }
            }.start();
        } else {
            Utility.showAlertToOpenGPS(mActivity);
        }

    }

    public void showProgressDialog() {
        dialog = new ProgressDialog(mActivity);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    public void hideProgressDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public void onStart() {
        super.onStart();
        if ((mGoogleApiClient != null) && (!mGoogleApiClient.isConnected())) {
            mGoogleApiClient.connect();
        }
    }

    public void onStop() {
        super.onStop();
        if ((mGoogleApiClient != null) && (mGoogleApiClient.isConnected())) {
            mGoogleApiClient.disconnect();
        }
    }

    private void dispatchTakePictureIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, IMAGECAPTURE_ACTIVITY);
        /*Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, IMAGECAPTURE_ACTIVITY);
        }*/
    }

    private void captureImage() {
        File dir;
        if (Utility.getFileDir(mActivity) != null && Utility.getFileDir(mActivity).length() > 0) {
            dir = new File(Utility.getFileDir(mActivity));
        } else {

//            File sdCard = Environment.getExternalStorageDirectory();
            PackageManager m = mActivity.getPackageManager();
            String s = mActivity.getPackageName();
            try {
                PackageInfo p = m.getPackageInfo(s, 0);
                s = p.applicationInfo.dataDir;
            } catch (PackageManager.NameNotFoundException e) {
                Log.w("yourtag", "Error Package name not found ", e);
                return;
            }

            File sdCard = new File(s);
            dir = new File(sdCard.getAbsolutePath() + "/iSave");

            if (!dir.exists()) {
                dir.mkdir();
            }

            Utility.setFileDir(dir.toString(), mActivity);
        }

        String uniqueId = Utility.getCurrentDate().replaceAll("/", "_") + "_" + System.currentTimeMillis() + ".png";
        current = uniqueId;
        mypath = new File(dir, current);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        uriSavedImage = Uri.fromFile(mypath);
        //intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);

        if (hasImageCaptureBug()) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        } else {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        }

        startActivityForResult(intent, IMAGECAPTURE_ACTIVITY);
    }

    public boolean hasImageCaptureBug() {

        // list of known devices that have the bug
        ArrayList<String> devices = new ArrayList<String>();
        devices.add("android-devphone1/dream_devphone/dream");
        devices.add("generic/sdk/generic");
        devices.add("vodafone/vfpioneer/sapphire");
        devices.add("tmobile/kila/dream");
        devices.add("verizon/voles/sholes");
        devices.add("google_ion/google_ion/sapphire");

        return devices.contains(android.os.Build.BRAND + "/" + android.os.Build.PRODUCT + "/"
                + android.os.Build.DEVICE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case IMAGECAPTURE_ACTIVITY:
                if (requestCode == IMAGECAPTURE_ACTIVITY && resultCode == Activity.RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(photo);
                    mCameraImg.setImageDrawable(bitmapDrawable);

                    try {
                        File dir;
                        if (Utility.getFileDir(mActivity) != null && Utility.getFileDir(mActivity).length() > 0) {
                            dir = new File(Utility.getFileDir(mActivity));
                        } else {

                            PackageManager m = mActivity.getPackageManager();
                            String s = mActivity.getPackageName();
                            try {
                                PackageInfo p = m.getPackageInfo(s, 0);
                                s = p.applicationInfo.dataDir;
                            } catch (PackageManager.NameNotFoundException e) {
                                Log.w("yourtag", "Error Package name not found ", e);
                                return;
                            }

                            File sdCard = new File(s);
                            dir = new File(sdCard.getAbsolutePath() + "/iSave");

                            if (!dir.exists()) {
                                dir.mkdir();
                            }

                            Utility.setFileDir(dir.toString(), mActivity);
                        } // boolean
                        //status = isExternalStoragePresent();

//                        String uniqueId = Utility.getCurrentDate().replaceAll("/", "_") + "_" + System.currentTimeMillis() + ".png";
                        String uniqueId = getText(emailIdEdt)+".png";
                        current = uniqueId;
                        System.out.println("current" + current);
                        mypath = new File(dir, current);
                        FileOutputStream mFileOutStream = new FileOutputStream(mypath);
                        photo.compress(Bitmap.CompressFormat.PNG, MyConstants.photoQuality, mFileOutStream); // Converting to byte array
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        photo.compress(Bitmap.CompressFormat.JPEG, MyConstants.photoQuality, stream);
                        imgFilePath = current;
                        Utility.setProfileImageURL(current, mActivity);
                        System.out.println("capture image  ===" + mypath);
                        mFileOutStream.flush();
                        mFileOutStream.close();

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    /*try {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 4;
                        final Bitmap bitmap = BitmapFactory.decodeFile(
                                uriSavedImage.getPath(), options);
                        FileOutputStream mFileOutStream;
                        try {
                            mFileOutStream = new FileOutputStream(
                                    uriSavedImage.getPath());
                            bitmap.compress(Bitmap.CompressFormat.PNG, 0, mFileOutStream);
                            mFileOutStream.flush();
                            mFileOutStream.close();
                            imgFilePath = current;
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        mCameraImg.setImageDrawable(new BitmapDrawable(bitmap));
                        //pending
                        //mCamera.setBackgroundDrawable(new BitmapDrawable(bitmap));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
                }
                break;
        }

    }

    private void populateData(){

        JobPortalDTO dto = (JobPortalDTO) JobportalDAo.getInstance().getRecords(DBHandler.getInstance(mActivity).getDBObject(0)).get(0);

        userNameEdt.setText(dto.getUserName());
        mobileNoEdt.setText(dto.getMobileNumber());
        emailIdEdt.setText(dto.getEmailAddress());
        KeySkillEdt.setText(dto.getKeySkills());
        reg_inter.setText(dto.getInterPercentage());
        reg_sscper.setText(dto.getSSCPercentage());
        reg_btech.setText(dto.getBtechPercentage());
preferedlocation.setText(dto.getPreferedLocation());
        reg_resume.setText(dto.getResumeHeadline());

    }

}
