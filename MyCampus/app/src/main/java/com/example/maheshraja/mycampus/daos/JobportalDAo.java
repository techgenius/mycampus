package com.example.maheshraja.mycampus.daos;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.maheshraja.mycampus.dtos.DTO;
import com.example.maheshraja.mycampus.dtos.JobPortalDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 01-08-2016.
 */
public class JobportalDAo implements DAO {
    private final String TAG = "userDAO";
    private static JobportalDAo jobportalDAO;
    public static JobportalDAo getInstance() {
        if (jobportalDAO == null) {
            jobportalDAO = new JobportalDAo();
        }

        return jobportalDAO;
    }

    public boolean insert(DTO dtoObject, SQLiteDatabase dbObject) {

        try {
            JobPortalDTO dto = (JobPortalDTO) dtoObject;

            ContentValues cValues = new ContentValues();
            cValues.put("userName", dto.getUserName());
            System.out.println("userName" + dto.getUserName());

            cValues.put("mobileNumber", dto.getMobileNumber());

            cValues.put("preferedLocation", dto.getPreferedLocation());

            cValues.put("emailAddress", dto.getEmailAddress());

            cValues.put("resumeHeadline", dto.getResumeHeadline());

            cValues.put("keySkills", dto.getKeySkills());

            cValues.put("SSCPercentage", dto.getSSCPercentage());

            cValues.put("interPercentage", dto.getInterPercentage());

            cValues.put("btechPercentage", dto.getBtechPercentage());
            dbObject.insert("FRIENDS_CNT", null, cValues);
            System.out.println("insert is:"+dbObject.insert("FRIENDS_CNT", null, cValues));
            return true;
        } catch (SQLException e) {
            Log.e(TAG + "insert()", e.getMessage());
            return false;

        } finally {
            dbObject.close();
        }

    }

    @Override
    public boolean update(DTO dtoObject, SQLiteDatabase dbObject) {
        try {
            JobPortalDTO dto = (JobPortalDTO) dtoObject;

            ContentValues cValues = new ContentValues();

            /*USER
            id
            name
            mobileNo
            email
            gender
            address
            uid
            uidType
            dob
            location
            */


            if (dto.getUserName() != null)
                cValues.put("userName", dto.getUserName());
            if (dto.getMobileNumber() != null)
                cValues.put("mobileNumber", dto.getMobileNumber());
            if (dto.getPreferedLocation() != null)
                cValues.put("preferedLocation", dto.getPreferedLocation());
            if (dto.getEmailAddress() != null)
                cValues.put("emailAddress", dto.getEmailAddress());
            if (dto.getResumeHeadline() != null)
                cValues.put("resumeHeadline", dto.getResumeHeadline());
            if (dto.getKeySkills() != null)
                cValues.put("keySkills", dto.getKeySkills());
            if (dto.getSSCPercentage() != null)
                cValues.put("SSCPercentage", dto.getSSCPercentage());
            if (dto.getInterPercentage() != null)
                cValues.put("interPercentage", dto.getInterPercentage());
            if (dto.getBtechPercentage() != null)
                cValues.put("btechPercentage", dto.getBtechPercentage());


            dbObject.update("FRIENDS_CNT", cValues, null, null);
            return true;
        } catch (SQLException e) {
            Log.e(TAG + "insert()", e.getMessage());
            return false;

        } finally {
            dbObject.close();
        }
    }


    @Override
    public boolean delete(DTO dtoObject, SQLiteDatabase dbObject) {
        return false;
    }

    @Override
    public List<DTO> getRecords(SQLiteDatabase dbObject) {
        List<DTO> userInfo = new ArrayList<DTO>();
        Cursor cursor = null;
        try {

            cursor = dbObject.rawQuery("SELECT * FROM FRIENDS_CNT", null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
           /*USER
            id
            name
            mobileNo
            email
            gender
            address
            uid
            uidType
            dob
            location
            */
                    JobPortalDTO dto = new JobPortalDTO();

                    //dto.setId(cursor.getLong(0));
                    dto.setUserName(cursor.getString(1));
                    dto.setMobileNumber(cursor.getString(2));
                    dto.setPreferedLocation(cursor.getString(3));
                    dto.setEmailAddress(cursor.getString(4));
                    dto.setResumeHeadline(cursor.getString(5));
                    dto.setKeySkills(cursor.getString(6));
                    dto.setSSCPercentage(cursor.getString(7));
                    dto.setInterPercentage(cursor.getString(8));
                    dto.setBtechPercentage(cursor.getString(9));

                    userInfo.add(dto);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            dbObject.close();

        }

        return userInfo;
    }

    @Override
    public List<DTO> getRecordInfoByValue(String columnName, String columnValue, SQLiteDatabase dbObject) {
        List<DTO> userInfo = new ArrayList<DTO>();
        Cursor cursor = null;
        try {
            if (!(columnName != null && columnName.length() > 0))
                columnName = "parentId";

            cursor = dbObject.rawQuery("SELECT * FROM FRIENDS_CNT where " + columnName + " = '" + columnValue + "'", null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
           /*USER
            id
            name
            mobileNo
            email
            gender
            address
            uid
            uidType
            dob
            location
            */
                    JobPortalDTO dto = new JobPortalDTO();

                    //dto.setId(cursor.getLong(0));
                    dto.setUserName(cursor.getString(1));
                    dto.setMobileNumber(cursor.getString(2));
                    dto.setPreferedLocation(cursor.getString(3));
                    dto.setEmailAddress(cursor.getString(4));
                    dto.setResumeHeadline(cursor.getString(5));
                    dto.setKeySkills(cursor.getString(6));
                    dto.setSSCPercentage(cursor.getString(7));
                    dto.setInterPercentage(cursor.getString(8));
                    dto.setBtechPercentage(cursor.getString(9));


                    userInfo.add(dto);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            dbObject.close();

        }

        return userInfo;
    }
    public boolean isDataAvailable(SQLiteDatabase dbObject) {
        Cursor cursor = null;
        try {

            cursor = dbObject.rawQuery("SELECT count(userName) FROM FRIENDS_CNT", null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                if (cursor.getInt(0) > 0)
                    return true;
                else
                    return false;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return false;
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            dbObject.close();
        }

        return false;
    }


    @Override
    public DTO getRecordsById(long Id, SQLiteDatabase dbObject) {
        return null;
    }
}
