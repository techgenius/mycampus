package com.example.maheshraja.mycampus.dtos;

import java.util.List;

/**
 * Created by MaheshRaja on 6/28/2016.
 */
public class SponserTypeDTO {

    String sponserType;
    List<SponserDTO> sponsers;

    public String getSponserType() {
        return sponserType;
    }

    public void setSponserType(String sponserType) {
        this.sponserType = sponserType;
    }

    public List<SponserDTO> getSponsers() {
        return sponsers;
    }

    public void setSponsers(List<SponserDTO> sponsers) {
        this.sponsers = sponsers;
    }


    public  String toString(){
        StringBuffer bf=new StringBuffer();
        bf.append("sponserType="+sponserType);
        bf.append("sponsers"+sponsers);



        return bf.toString();
    }
}
