package com.example.maheshraja.mycampus.Activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.maheshraja.mycampus.R;

/**
 * Created by lenovo on 27-07-2016.
 */
public class IshareWebview  extends  BaseFragment{
    WebView mWebView;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.isharewebview, container, false);

        mWebView = (WebView)rootView. findViewById(R.id.webview);

        String url = "http://192.168.1.8:8080/Ishare/home.jsp";
        // probably a good idea to check it's not null, to avoid these situations:
        if(mWebView != null){
            mWebView.loadUrl(url);
            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }
            });
        }
        return rootView;
    }
}
