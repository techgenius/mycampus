package com.example.maheshraja.mycampus.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.maheshraja.mycampus.Activity.EventsFragment;
import com.example.maheshraja.mycampus.Activity.MainActivity;
import com.example.maheshraja.mycampus.R;


public class TextviewAdapter implements android.widget.ListAdapter {
    protected MainActivity mActivity;
    private Context mContext;

    // Constructor
    public TextviewAdapter(EventsFragment c) {
        mContext = mActivity;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;

        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        }
        else
        {
            imageView = (ImageView) convertView;
        }
        imageView.setImageResource(mThumbIds[position]);
        return imageView;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    // Keep all Images in array
    public Integer[] mThumbIds = {
            R.drawable.ic_accident, R.drawable.ic_accident,
            R.drawable.ic_accident, R.drawable.ic_accident,
            R.drawable.ic_accident, R.drawable.ic_accident,
            R.drawable.ic_accident, R.drawable.ic_accident,
            R.drawable.ic_accident, R.drawable.ic_accident,
            R.drawable.ic_accident, R.drawable.ic_accident,
            R.drawable.ic_accident, R.drawable.ic_accident,
            R.drawable.ic_accident, R.drawable.ic_accident,
            R.drawable.ic_accident, R.drawable.ic_accident,
            R.drawable.ic_accident, R.drawable.ic_accident,
            R.drawable.ic_accident, R.drawable.ic_accident
    };

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }
}
