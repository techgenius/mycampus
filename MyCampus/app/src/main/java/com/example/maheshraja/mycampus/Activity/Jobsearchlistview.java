package com.example.maheshraja.mycampus.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.adapter.JobserachlistAdapter;
import com.example.maheshraja.mycampus.dtos.JobPortalDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 29-07-2016.
 */
public class Jobsearchlistview  extends BaseFragment{
    private  Context context;
    List<JobPortalDTO> myStrings=null;
    List<JobPortalDTO> list1 = new ArrayList<>();
    JobserachlistAdapter adapter;
    ListView tx;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        context=mActivity;
        View rootView = inflater.inflate(R.layout.jobsearch_list, container, false);
        Bundle bundle = getArguments();
        myStrings = (List<JobPortalDTO>) bundle.getSerializable("array");
        System.out.println("its mine"+myStrings);


           tx=  (ListView) rootView.findViewById(R.id.list_view);
            adapter = new JobserachlistAdapter(myStrings,context);
            tx.setAdapter(adapter);

      tx.setOnItemClickListener(new AdapterView.OnItemClickListener() {
          @Override
          public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


              FragmentTransaction transection = getFragmentManager().beginTransaction();
              JobPortalSearchview mfragment = new JobPortalSearchview();



              Bundle bundle = new Bundle();
          bundle.putInt("pos", position);
              JobPortalDTO job=myStrings.get(position);
              bundle.putString("company_name",job.getCompany_name());
              bundle.putString("company_loc",job.getCompany_loc());
              bundle.putString("keyword",job.getKeyword());
              bundle.putString("tagline",job.getTagline());
              bundle.putString("Interview_loc",job.getInterview_loc());
              bundle.putString("wanted_exp",job.getWanted_exp());
              bundle.putString("jobdescription",job.getJobdescription());
              bundle.putString("jobid",job.getJobId());
              mfragment.setArguments(bundle); //data being send to SecondFragment
              transection.replace(R.id.container_body, mfragment);
              transection.commit();


          }
      });


        /*LinearLayout menu_photos = (LinearLayout )rootView.findViewById(R.id.linear_layout);
        menu_photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId()==R.id.linear_layout) {
                    Toast.makeText(getContext(), "Item first postion is:" , Toast.LENGTH_LONG).show();

                    FragmentTransaction transection = getFragmentManager().beginTransaction();
                    JobPortalSearchview mfragment = new JobPortalSearchview();
//using Bundle to send data
                    Bundle bundle = new Bundle();
                    //bundle.putStringArrayList("mylist",list1);
                    bundle.putSerializable("array", (java.io.Serializable) myStrings);
                    mfragment.setArguments(bundle); //data being send to SecondFragment
                    transection.replace(R.id.container_body, mfragment);
                    transection.commit();

                }
            }
        });*/

        if(myStrings.size()>0)
            {
                populateData();
            }



            return rootView;
        }

public void populateData()
{
    adapter.notifyDataSetChanged();
}
}
