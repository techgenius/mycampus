package com.example.maheshraja.mycampus.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.daos.UserDAO;
import com.example.maheshraja.mycampus.database.DBHandler;


public class SplashScreenActivity extends AppCompatActivity {

    private CountDownTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_splash_screen);

       // appNavigation();

        startTimer();


    }

    private void startTimer(){
        timer = new CountDownTimer(1000, 3000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                appNavigation();
            }
        };
        timer.start();
    }

    private void appNavigation(){
       boolean dataAvailable = UserDAO.getInstance().isDataAvailable(DBHandler.getInstance(SplashScreenActivity.this).getDBObject(0));

        if(dataAvailable) {
            //intent = new Intent(SplashScreenActivity.this, MainActivity.class);

            Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
            // }
        }
        else {
            Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if(timer != null)
            timer.cancel();
        super.onBackPressed();
    }
}
