package com.example.maheshraja.mycampus.daos;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.maheshraja.mycampus.dtos.DTO;
import com.example.maheshraja.mycampus.dtos.FriendsDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Swamy on 3/5/2016.
 */
public class FriendsDAO implements DAO {
    private final String TAG = "userDAO";
    private static FriendsDAO friendsDAO;


    public static FriendsDAO getInstance() {
        if (friendsDAO == null) {
            friendsDAO = new FriendsDAO();
        }

        return friendsDAO;
    }

    /**
     * delete the Data
     */



    public boolean delete(DTO dtoObject, SQLiteDatabase dbObject) {
        /*FRIENDS_CNT
          id
          name
          mobileNo
        */

        try {
            dbObject.compileStatement("DELETE FROM FRIENDS_CNT").execute();
            return true;
        } catch (Exception e) {
            Log.e(TAG + "delete()", e.getMessage());
        }finally {
            dbObject.close();
        }
        return false;
    }

    public boolean deleteAll(SQLiteDatabase dbObject) {
        /*FRIENDS_CNT
          id
          name
          mobileNo
        */

        try {
            dbObject.compileStatement("DELETE FROM FRIENDS_CNT").execute();
            return true;
        } catch (Exception e) {
            Log.e(TAG + "delete()", e.getMessage());
        }finally {
            dbObject.close();
        }
        return false;
    }


    @Override
    public DTO getRecordsById(long id, SQLiteDatabase dbObject) {
        return null;
    }

    /**
     * Inserts the data in the SQLite database
     *
     * @param dbObject  : Exposes methods to manage a SQLite database Object
     * @param dtoObject : DTO object is passed
     */

    public boolean insert(DTO dtoObject, SQLiteDatabase dbObject) {

        try {
            FriendsDTO dto = (FriendsDTO) dtoObject;

            ContentValues cValues = new ContentValues();

            /*FRIENDS_CNT
              id
              name
              mobileNo
            */


            cValues.put("name", dto.getName());
            cValues.put("mobileNo", dto.getMobileNo());

            dbObject.insert("FRIENDS_CNT", null, cValues);
            return true;
        } catch (SQLException e) {
            Log.e(TAG + "insert()", e.getMessage());
            return false;

        } finally {
            dbObject.close();
        }

    }


    @Override
    public boolean update(DTO dtoObject, SQLiteDatabase dbObject) {

        try {
            FriendsDTO dto = (FriendsDTO) dtoObject;

            ContentValues cValues = new ContentValues();

            /*FRIENDS_CNT
              id
              name
              mobileNo
            */


            if (dto.getName() != null)
                cValues.put("name", dto.getName());
            if (dto.getMobileNo() != null)
                cValues.put("mobileNo", dto.getMobileNo());

            dbObject.update("FRIENDS_CNT", cValues, null, null);
            return true;
        } catch (SQLException e) {
            Log.e(TAG + "update()", e.getMessage());
            return false;

        } finally {
            dbObject.close();
        }
    }


    @Override
    public List<DTO> getRecords(SQLiteDatabase dbObject) {
        List<DTO> userInfo = new ArrayList<DTO>();
        Cursor cursor = null;
        try {

            cursor = dbObject.rawQuery("SELECT * FROM FRIENDS_CNT", null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {

                    FriendsDTO dto = new FriendsDTO();

                    dto.setId(cursor.getLong(0));
                    dto.setName(cursor.getString(1));
                    dto.setMobileNo(cursor.getString(2));

                    userInfo.add(dto);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            dbObject.close();

        }

        return userInfo;
    }

    @Override
    public List<DTO> getRecordInfoByValue(String columnName, String columnValue, SQLiteDatabase dbObject) {
        List<DTO> userInfo = new ArrayList<DTO>();
        Cursor cursor = null;
        try {
            if (!(columnName != null && columnName.length() > 0))
                columnName = "parentId";

            cursor = dbObject.rawQuery("SELECT * FROM FRIENDS_CNT where " + columnName + " = '" + columnValue + "'", null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {

                    FriendsDTO dto = new FriendsDTO();

                    dto.setId(cursor.getLong(0));
                    dto.setName(cursor.getString(1));
                    dto.setMobileNo(cursor.getString(2));

                    userInfo.add(dto);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            dbObject.close();

        }

        return userInfo;
    }

    public boolean isDataAvailable(SQLiteDatabase dbObject) {
        Cursor cursor = null;
        try {

            cursor = dbObject.rawQuery("SELECT count(name) FROM FRIENDS_CNT", null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                if (cursor.getInt(0) > 0)
                    return true;
                else
                    return false;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return false;
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            dbObject.close();
        }

        return false;
    }
}
