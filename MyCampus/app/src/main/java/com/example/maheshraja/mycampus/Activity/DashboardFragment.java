package com.example.maheshraja.mycampus.Activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.adapter.CustomAdapter;
import com.example.maheshraja.mycampus.utility.MyConstants;

import java.util.ArrayList;


public class DashboardFragment extends BaseFragment{
    protected AlphaAnimation fadeIn = new AlphaAnimation(0.0f, 1.0f);

    TextView text;
    GridView gridview;
    ArrayList<String> mItems;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        gridview = (GridView) rootView.findViewById(R.id.grid_view);
        gridview.setAdapter(new CustomAdapter(rootView.getContext()));

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // text.setText((String) (gridview.getItemAtPosition(position)));
                Toast.makeText(getContext(), "ITEM_CLICKED" + (String) (gridview.getItemAtPosition(position)), Toast.LENGTH_LONG).show();

                switch (position) {
                    case 0:
                        Toast.makeText(getContext(), "first ITEM_CLICKED", Toast.LENGTH_LONG).show();
                        mActivity.pushFragments(MyConstants.TAB_HOME, new IshareWebview(), false, true);
                        break;
                    case 1:
                        mActivity.pushFragments(MyConstants.TAB_HOME, new PoliceFragment(), false, true);
                        break;
                    case 2:
                        mActivity.pushFragments(MyConstants.TAB_HOME, new ConfenssionList(), false, true);
                        break;
                    case 3:
                       mActivity.pushFragments(MyConstants.TAB_HOME, new JobportalForm(), false, true);
                       /* Intent phoneIntent = new Intent(getContext(), BackpressActivity.class);
                               startActivity(phoneIntent);*/
                        break;
                    case 4:
                        mActivity.pushFragments(MyConstants.TAB_HOME, new kaletechgenius(), false, true);
                       /* Intent phoneIntent = new Intent(getContext(), BackpressActivity.class);
                               startActivity(phoneIntent);*/
                        break;
                    case 5:
                        mActivity.pushFragments(MyConstants.TAB_HOME, new OnlineTrain(), false, true);
                       /* Intent phoneIntent = new Intent(getContext(), BackpressActivity.class);
                               startActivity(phoneIntent);*/
                        break;
                }


            }


        });

        return rootView;
    }


}