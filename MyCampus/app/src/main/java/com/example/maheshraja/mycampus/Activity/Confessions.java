package com.example.maheshraja.mycampus.Activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.asynctask.MyAsyncTask;
import com.example.maheshraja.mycampus.asynctask.SpinnerAsyncTask;
import com.example.maheshraja.mycampus.daos.UserDAO;
import com.example.maheshraja.mycampus.database.DBHandler;
import com.example.maheshraja.mycampus.dtos.ConfessionDTO;
import com.example.maheshraja.mycampus.dtos.DTO;
import com.example.maheshraja.mycampus.dtos.UserDTO;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 15-07-2016.
 */
public class Confessions extends  BaseFragment {
    private EditText edit;
    String ID;
    private Context context;
    List<ConfessionDTO> list = new ArrayList<>();
    List<String> lables = new ArrayList<>();
    ;

    //List<String> list= new ArrayList<String>();

    ConfessionDTO st;
    ArrayAdapter<String> spinnerAdapter;
String contype;
    String conmsg;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)  {

        context = mActivity;
      /*  String colors[] = {"Red","Blue","White","Yellow","Black", "Green","Purple","Orange","Grey"};
        String ss[]=*/

        View rootView = inflater.inflate(R.layout.fragment_condenssion, container, false);
        TextView text=(TextView) rootView.findViewById(R.id.text);
        TextView text1=(TextView) rootView.findViewById(R.id.textview1);
       edit=(EditText) rootView.findViewById(R.id.message);
        Button button;

        final Spinner type = (Spinner) rootView.findViewById(R.id.list);
        System.out.println("hai");
        getSponsers();

        System.out.println("hello");


        System.out.println("swarupa is:" + lables);



        spinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, lables);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        type.setAdapter(spinnerAdapter);
        type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("" + parent.getItemAtPosition(position).toString());
                Toast.makeText(context, "onitem click postion" + parent.getItemAtPosition(position).toString(), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        button = (Button) rootView.findViewById(R.id.submit);



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit.getText().toString().trim().length() > 0 ) {
                    Toast.makeText(context,"OnClickListener : " + "\nSpinner 1 : " + String.valueOf(type.getSelectedItem()),Toast.LENGTH_SHORT).show();
                    contype=String.valueOf(type.getSelectedItem());
                    System.out.println("contype is:" + contype);
                    conmsg=edit.getText().toString().trim();
                    sendData();
                } else {
                    Utility.showAlert(mActivity, null, "Please enter your message");
                }
            }
        });




        return rootView;
    }
   /* private void sendData(){
        String url = null;
        String msg = null;

                url = MyConstants.RAGGING_URL;
                msg = getMessageForRagging(edtMsg.getText().toString().trim());


        }*/
   private void sendData() {

       ConfessionDTO dto = (ConfessionDTO) getUserData();
       UserDTO user = (UserDTO) UserDAO.getInstance().getRecords(DBHandler.getInstance(context).getDBObject(0)).get(0);
       JSONObject reqJson = new JSONObject();

       try {
           reqJson.put("college_name", dto.getCollege_name());
           reqJson.put("data", dto.getData());
           reqJson.put("user_name", user.getEmailId());
       } catch (JSONException e) {
           e.printStackTrace();
       }catch (Exception e){

       }

       MyAsyncTask loginAsync = new MyAsyncTask(reqJson.toString(), taskCompleteListener, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
       Utility.showProgressDialog(context);
       loginAsync.execute(MyConstants.Confession_URL);

   }

    private DTO getUserData() {

        ConfessionDTO dto = new ConfessionDTO();
        dto.setCollege_name(contype);
        dto.setData(conmsg);
        return  dto;
    }
    private void getSponsers() {
        System.out.println("getSponser is calling");
        SpinnerAsyncTask sponser = new SpinnerAsyncTask(completeListener, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
        Utility.showProgressDialog(context);
        sponser.execute(MyConstants.ADVT_URL1);
    }

    private void populateData() {

        spinnerAdapter.notifyDataSetChanged();
    }


    private AsyncTaskCompleteListener completeListener = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result) {
            if (result != null && !result.isEmpty()) {
                if (Utility.isJSONValid(result)) {
                    System.out.println("test AsyncTask for retrive");
                    try {


                        JSONArray ar = new JSONArray(result);
                        if (ar != null && ar.length() > 0) {
                            for (int i = 0; i < ar.length(); i++) {
                                JSONObject obj = ar.getJSONObject(i);
                                ConfessionDTO dto = new ConfessionDTO();
                                dto.setCollege_name(obj.getString("college_name"));
                                System.out.println("ar=" + ar);


                                list.add(dto);
                            }
                            if (list.size() > 0) {
                                for (int ii = 0; ii < list.size(); ii++) {
                                    lables.add(list.get(ii).getCollege_name());
                                    populateData();
                                    System.out.println("display lable" + lables);
                                }


                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {
                    //Utility.showAlert(LoginActivity.this, null, "Invalid Response contact system admin");
                    //Utility.showAlert(SponsorsFragment.this, null, "Invali userId/password");
                }

            } else {
                //Utility.showAlert(LoginActivity.this, "Network problem", "Check your internet connection");
                // Utility.showAlert(LoginActivity.this, null, "Invali userId/password");
            }
            Utility.hideProgressDialog();
        }
    };

/*public List display(){

    for (int ii = 0; ii < list.size(); ii++) {
        lables.add(list.get(ii).getType());

        System.out.println("display lable" + lables);
    }

 return lables;
}*/
private void clearForm(){
    edit.setText("");
    //surakshaRadioGroup.clearCheck();
}

    private AsyncTaskCompleteListener taskCompleteListener = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result1) {
            if (result1 != null) {
                if(Utility.isJSONValid(result1)){
                    try {
                        System.out.println("test AsyncTask for insert");
                        JSONObject object = new JSONObject(result1);
                        int code = object.optInt("code");
                        if(code == 100){
                            //navigateApp();
                            clearForm();
                            Utility.showAlert(context, null, "Post Confession Sucessfully");
                        }else if(code == 101){
                            Utility.showAlert(context, null, "Failed to Insert");
                        }else{
                            Utility.showAlert(context, null, "Unknown response");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Utility.showAlert(context, null, "Invalid Response contact system admin");
                }

            } else {
                Utility.showAlert(context, "Network problem", "Check your internet connection");
            }
            Utility.hideProgressDialog();
        }
    };

}