package com.example.maheshraja.mycampus.Activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.adapter.JobserachlistAdapter;
import com.example.maheshraja.mycampus.dtos.JobPortalDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 01-08-2016.
 */
public class JobAppliedsearchlist extends  BaseFragment {
    private Context context;
    List<JobPortalDTO> myStrings = null;
    List<JobPortalDTO> list1 = new ArrayList<>();
    JobserachlistAdapter adapter;
    String jobId;
    Button button;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        context = mActivity;
        View rootView = inflater.inflate(R.layout.fragement_apply_list, container, false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            String CompanyName = getArguments().getString("company_name");
            System.out.println(CompanyName);

            String CompanyLoc = getArguments().getString("company_loc");
            System.out.println(CompanyLoc);

            String tagline = getArguments().getString("tagline");
            System.out.println(tagline);

            String keyword = getArguments().getString("keyword");
            System.out.println(keyword);

            String wantedexp = getArguments().getString("wanted_exp");
            System.out.println(wantedexp);

            String interview_loc = getArguments().getString("Interview_loc");
            System.out.println(interview_loc);


            String jobdescription = getArguments().getString("jobdescription");
            System.out.println(jobdescription);

            jobId = getArguments().getString("jobid");
            System.out.println(jobdescription);

            // String CompanyName=getArguments().getString("company_name");
            final TextView text = (TextView) rootView.findViewById(R.id.subtextview0);
            final TextView text1 = (TextView) rootView.findViewById(R.id.subtextview1);
            final TextView text2 = (TextView) rootView.findViewById(R.id.subtextview2);
            final TextView text3 = (TextView) rootView.findViewById(R.id.subtextview3);
            final TextView text4 = (TextView) rootView.findViewById(R.id.subtextview4);

            text.setText(tagline);
            text1.setText(CompanyName);
            text2.setText(CompanyLoc);
            text3.setText(keyword);
            text4.setText(wantedexp);




        }
        return rootView;

    }
}
