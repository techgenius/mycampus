package com.example.maheshraja.mycampus.Activity;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.adapter.CustomAdapter;
import com.example.maheshraja.mycampus.daos.JobportalDAo;
import com.example.maheshraja.mycampus.database.DBHandler;
import com.example.maheshraja.mycampus.utility.MyConstants;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class AboutApp2Fragment extends BaseFragment {
    protected AlphaAnimation fadeIn = new AlphaAnimation(0.0f, 1.0f);

    TextView text;
    GridView gridview;
    ArrayList<String> mItems;
    private Context context;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        gridview = (GridView) rootView.findViewById(R.id.grid_view);
        gridview.setAdapter(new CustomAdapter(rootView.getContext()));
context=mActivity;
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // text.setText((String) (gridview.getItemAtPosition(position)));
                Toast.makeText(getContext(), "ITEM_CLICKED" + (String) (gridview.getItemAtPosition(position)), Toast.LENGTH_LONG).show();

                switch (position) {
                    case 0:
                        Toast.makeText(getContext(), "first ITEM_CLICKED", Toast.LENGTH_LONG).show();
                        mActivity.pushFragments(MyConstants.TAB_HOME, new IshareWebview(), false, true);
                        break;
                    case 1:
                        mActivity.pushFragments(MyConstants.TAB_HOME, new PoliceFragment(), false, true);
                        break;
                    case 2:
                        mActivity.pushFragments(MyConstants.TAB_HOME, new ConfenssionList(), false, true);
                        break;
                    case 3:


                        boolean dataAvailable = JobportalDAo.getInstance().isDataAvailable(DBHandler.getInstance(getContext()).getDBObject(0));
                        System.out.println(""+dataAvailable);

                     /*   JobPortalDTO user = (JobPortalDTO) JobportalDAo.getInstance().getRecords(DBHandler.getInstance(context).getDBObject(0)).get(0);
                        JSONObject reqJson = new JSONObject();
                       System.out.println("user Emailid:"+user.getEmailAddress());
*/
                        if (dataAvailable)
                            mActivity.pushFragments(MyConstants.TAB_HOME, new Jobportal(), false, true);
                        else
                            mActivity.pushFragments(MyConstants.TAB_HOME, new JobportalForm(), false, true);
                       /* Intent phoneIntent = new Intent(getContext(), BackpressActivity.class);
                               startActivity(phoneIntent);*/
                        break;
                    case 4:
                        mActivity.pushFragments(MyConstants.TAB_HOME, new kaletechgenius(), false, true);
                       /* Intent phoneIntent = new Intent(getContext(), BackpressActivity.class);
                               startActivity(phoneIntent);*/
                        break;
                    case 5:
                        mActivity.pushFragments(MyConstants.TAB_HOME, new OnlineTrain(), false, true);
                       /* Intent phoneIntent = new Intent(getContext(), BackpressActivity.class);
                               startActivity(phoneIntent);*/
                        break;
                }


            }


        });

        return rootView;
    }

}
