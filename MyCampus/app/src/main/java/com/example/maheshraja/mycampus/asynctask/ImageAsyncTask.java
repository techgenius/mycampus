package com.example.maheshraja.mycampus.asynctask;

/**
 * Created by Swamy on 3/4/2016.
 */

import android.content.Context;
import android.os.AsyncTask;


import com.example.maheshraja.mycampus.daos.UserDAO;
import com.example.maheshraja.mycampus.database.DBHandler;
import com.example.maheshraja.mycampus.dtos.UserDTO;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;

import java.io.File;
import java.io.IOException;
import java.util.List;


/**
 * Created by Yakaswamy.g on 2/22/2016.
 */

public class ImageAsyncTask extends AsyncTask<String, Void, String> {

    private String jsonData;
    private AsyncTaskCompleteListener asyncTaskCompleteListener;
    private int connectionTimeout;
    private int socketTimeout;
    private String imageUrl;
    private Context context;
    private com.example.maheshraja.mycampus.utility.MyConstants MyConstants;

    /* public ImageAsyncTask(Context context, String imageUrl, AsyncTaskCompleteListener asyncTaskCompleteListener, int connectionTimeout, int socketTimeout) {
         this.imageUrl = imageUrl;
         this.asyncTaskCompleteListener = asyncTaskCompleteListener;
         this.connectionTimeout = connectionTimeout;
         this.socketTimeout = socketTimeout;
         this.context = context;
     }
 */
    public ImageAsyncTask(Context context, String imageUrl, AsyncTaskCompleteListener asyncTaskCompleteListener, int connectionTimeout, int socketTimeout) {
        this.imageUrl = imageUrl;
        this.asyncTaskCompleteListener = asyncTaskCompleteListener;
        this.connectionTimeout = connectionTimeout;
        this.socketTimeout = socketTimeout;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {

        String charset = "UTF-8";
        File uploadFile1 = new File(imageUrl);
        //File uploadFile2 = new File("e:/Test/PIC2.JPG");
        String requestURL = MyConstants.APP_URL+params[0];  //"http://localhost:8080/FileUploadSpringMVC/uploadFile.do";

        try {
            MultipartUtility multipart = new MultipartUtility(requestURL, charset);

            multipart.addHeaderField("User-Agent", "CodeJava");
            multipart.addHeaderField("Test-Header", "Header-Value");

            UserDTO user = (UserDTO) UserDAO.getInstance().getRecords(DBHandler.getInstance(context).getDBObject(0)).get(0);

            multipart.addFormField("userid", user.getEmailId());
            multipart.addFormField("password", user.getPassword());

            multipart.addFilePart("fileUpload", uploadFile1);
            //multipart.addFilePart("fileUpload", uploadFile2);

            List<String> response = multipart.finish();

            System.out.println("SERVER REPLIED:");

            for (String line : response) {
                System.out.println(line);
                return line;
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        asyncTaskCompleteListener.onAsynComplete(result);
    }
}


